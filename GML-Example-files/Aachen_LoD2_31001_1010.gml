<?xml version='1.0' encoding='UTF-8' standalone='yes'?>
<core:CityModel xmlns:core="http://www.opengis.net/citygml/2.0" xmlns:gen="http://www.opengis.net/citygml/generics/2.0" xmlns:grp="http://www.opengis.net/citygml/cityobjectgroup/2.0" xmlns:app="http://www.opengis.net/citygml/appearance/2.0" xmlns:bldg="http://www.opengis.net/citygml/building/2.0" xmlns:gml="http://www.opengis.net/gml" xmlns:xal="urn:oasis:names:tc:ciq:xsdschema:xAL:2.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <gml:name>created by the CGML-ATB of the e3D</gml:name>
  <gml:boundedBy>
    <gml:Envelope srsName="urn:adv:crs:ETRS89_UTM32*DE_DHHN2016_NH*GCG2016">
      <gml:lowerCorner srsDimension="3">294065.165 5629374.545 175.566</gml:lowerCorner>
      <gml:upperCorner srsDimension="3">294819.163 5629961.911 197.392</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <core:cityObjectMember>
    <bldg:Building gml:id="DENW39AL1000MjqW">
      <core:creationDate>2018-12-21</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
        <core:externalObject>
          <core:name>DENW39AL1000MjqW</core:name>
        </core:externalObject>
      </core:externalReference>
      <gen:stringAttribute name="Gemeindeschluessel">
        <gen:value>05334002</gen:value>
      </gen:stringAttribute>
      <bldg:function>31001_1010</bldg:function>
      <bldg:yearOfConstruction>1966</bldg:yearOfConstruction>
      <bldg:consistsOfBuildingPart>
        <bldg:BuildingPart gml:id="UUID_d59831ca-5c4c-4690-a522-b6e8311e46b2">
          <core:creationDate>2018-12-21</core:creationDate>
          <gen:stringAttribute name="DatenquelleDachhoehe">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleBodenhoehe">
            <gen:value>1100</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleLage">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <bldg:roofType>3100</bldg:roofType>
          <bldg:measuredHeight uom="urn:adv:uom:m">17.766</bldg:measuredHeight>
          <bldg:lod2Solid>
            <gml:Solid>
              <gml:exterior>
                <gml:CompositeSurface>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_0"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_9"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_6"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_4"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_5"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_8"/>
                  <gml:surfaceMember xlink:href="#UUID_e079ae16-634e-449c-9653-37df687826a1"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_7"/>
                  <gml:surfaceMember xlink:href="#UUID_e73a202a-8fa1-46e2-972f-6a5104b985bb"/>
                  <gml:surfaceMember xlink:href="#UUID_f83f08e8-190b-44ba-ad59-26332670500f"/>
                  <gml:surfaceMember xlink:href="#UUID_4985c59f-5582-4487-ad94-c04e63f033c9"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179274_2_1"/>
                </gml:CompositeSurface>
              </gml:exterior>
            </gml:Solid>
          </bldg:lod2Solid>
          <bldg:lod2TerrainIntersection>
            <gml:MultiCurve>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294090.252 5629753.202 190.404 294090.471 5629753.0 190.421 294090.747 5629752.747 190.415 294091.0 5629752.514 190.424 294091.267 5629752.267 190.431 294091.558 5629752.0 190.439 294091.788 5629751.788 190.434 294092.0 5629751.593 190.434 294092.309 5629751.309 190.434 294092.645 5629751.0 190.434 294092.83 5629750.83 190.432 294093.0 5629750.673 190.43 294093.35 5629750.35 190.433 294093.731 5629750.0 190.43 294093.871 5629749.871 190.429 294094.0 5629749.752 190.43 294094.392 5629749.392 190.434 294094.818 5629749.0 190.43 294094.913 5629748.913 190.43 294095.0 5629748.832 190.43 294095.433 5629748.433 190.434 294095.904 5629748.0 190.43 294095.954 5629747.954 190.43 294096.0 5629747.912 190.431 294096.475 5629747.475 190.435 294096.991 5629747.0 190.43 294096.996 5629746.996 190.43 294097.0 5629746.991 190.43 294097.045 5629746.95 190.43</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294090.252 5629753.202 190.404 294090.811 5629753.811 190.302 294090.985 5629754.0 190.27 294091.0 5629754.017 190.269 294091.902 5629755.0 190.273 294092.0 5629755.106 190.268 294092.82 5629756.0 190.195 294093.0 5629756.196 190.192 294093.738 5629757.0 190.204 294094.0 5629757.285 190.223 294094.656 5629758.0 190.256 294094.934 5629758.302 190.27</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294094.934 5629758.302 190.27 294095.0 5629758.374 190.274 294095.574 5629759.0 190.297 294096.0 5629759.464 190.319 294096.492 5629760.0 190.34 294097.0 5629760.553 190.361 294097.41 5629761.0 190.382 294098.0 5629761.643 190.419 294098.328 5629762.0 190.446 294099.0 5629762.732 190.509 294099.246 5629763.0 190.53 294099.615 5629763.402 190.529</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294099.615 5629763.402 190.529 294100.0 5629763.047 190.558 294100.024 5629763.024 190.559 294100.051 5629763.0 190.559 294100.545 5629762.545 190.522 294101.0 5629762.125 190.409 294101.065 5629762.065 190.398 294101.136 5629762.0 190.408 294101.585 5629761.585 190.52 294102.0 5629761.203 190.536 294102.106 5629761.106 190.535 294102.22 5629761.0 190.533 294102.626 5629760.626 190.517 294103.0 5629760.281 190.524 294103.146 5629760.146 190.523 294103.305 5629760.0 190.515 294103.667 5629759.667 190.49 294104.0 5629759.359 190.506 294104.187 5629759.187 190.509 294104.39 5629759.0 190.508 294104.707 5629758.707 190.487 294105.0 5629758.437 190.479 294105.228 5629758.228 190.468 294105.475 5629758.0 190.475 294105.748 5629757.748 190.477 294106.0 5629757.516 190.495 294106.268 5629757.268 190.497 294106.559 5629757.0 190.493 294106.789 5629756.789 190.474 294107.0 5629756.594 190.464 294107.309 5629756.309 190.437 294107.644 5629756.0 190.44 294107.829 5629755.829 190.44 294108.0 5629755.672 190.443 294108.35 5629755.35 190.443 294108.729 5629755.0 190.45 294108.87 5629754.87 190.447 294109.0 5629754.75 190.45 294109.39 5629754.39 190.438 294109.813 5629754.0 190.426 294109.91 5629753.91 190.42 294110.0 5629753.828 190.422 294110.431 5629753.431 190.413 294110.898 5629753.0 190.421 294110.951 5629752.951 190.42 294111.0 5629752.906 190.417 294111.154 5629752.764 190.407</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294106.463 5629747.679 190.356 294106.76 5629748.0 190.362 294107.0 5629748.261 190.363 294107.682 5629749.0 190.37 294108.0 5629749.345 190.377 294108.604 5629750.0 190.384 294109.0 5629750.429 190.389 294109.527 5629751.0 190.395 294110.0 5629751.513 190.4 294110.449 5629752.0 190.401 294111.0 5629752.597 190.408 294111.154 5629752.764 190.407</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294101.773 5629742.594 190.292 294102.0 5629742.84 190.302 294102.148 5629743.0 190.309 294103.0 5629743.924 190.318 294103.07 5629744.0 190.32 294103.901 5629744.901 190.338 294103.992 5629745.0 190.34 294104.0 5629745.008 190.34 294104.915 5629746.0 190.333 294105.0 5629746.092 190.332 294105.837 5629747.0 190.342 294106.0 5629747.177 190.345 294106.463 5629747.679 190.356</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294097.045 5629746.95 190.43 294097.516 5629746.516 190.43 294098.0 5629746.07 190.411 294098.037 5629746.037 190.41 294098.076 5629746.0 190.409 294098.557 5629745.557 190.4 294099.0 5629745.149 190.391 294099.077 5629745.077 190.39 294099.162 5629745.0 190.385 294099.598 5629744.598 190.364 294100.0 5629744.228 190.352 294100.118 5629744.118 190.352 294100.247 5629744.0 190.35 294100.639 5629743.639 190.346 294101.0 5629743.306 190.322 294101.159 5629743.159 190.313 294101.332 5629743.0 190.31 294101.68 5629742.68 190.297 294101.773 5629742.594 190.292</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:MultiCurve>
          </bldg:lod2TerrainIntersection>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_aa848b41-d020-44b4-a71c-4010a48a8eef">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_ff2fd0d5-e32d-47b6-88d4-d94eb51c2b71">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_1">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_1_0_">
                          <gml:posList srsDimension="3">294097.045 5629746.95 204.04 294090.252 5629753.202 204.039 294090.252 5629753.202 190.192 294097.045 5629746.95 190.192 294097.045 5629746.95 204.04</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_2f5c7221-a2ba-4d0f-94ff-ff8579e7eb14">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_d407c975-0bfd-41e4-b68b-72c3debdf8cb">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_4">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_4_0_">
                          <gml:posList srsDimension="3">294099.615 5629763.402 204.039 294111.154 5629752.764 204.039 294111.154 5629752.764 190.192 294099.615 5629763.402 190.192 294099.615 5629763.402 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_ca4689d4-c135-4b66-ba50-ee22fbe13021">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_d8659ca0-f6e3-44a7-af0e-b82d807218ab">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_5">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_5_0_">
                          <gml:posList srsDimension="3">294111.154 5629752.764 204.039 294106.463 5629747.679 207.959 294106.463 5629747.679 190.192 294111.154 5629752.764 190.192 294111.154 5629752.764 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_beb79b05-31bd-449d-865a-949afcd3e2c8">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_d4c242a9-24fa-4ca2-8718-6581091ead3f">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_6">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_6_0_">
                          <gml:posList srsDimension="3">294106.463 5629747.679 207.959 294101.773 5629742.594 204.039 294101.773 5629742.594 190.192 294106.463 5629747.679 190.192 294106.463 5629747.679 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_27cf0ec5-7797-42d4-b419-c680c38fc04c">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_ce8d95eb-23bf-4837-b810-7b8bef7fc2da">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_7">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_7_0_">
                          <gml:posList srsDimension="3">294101.773 5629742.594 204.039 294097.045 5629746.95 204.04 294097.045 5629746.95 190.192 294101.773 5629742.594 190.192 294101.773 5629742.594 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_2574761f-229e-49f9-87d2-c0b101f7c3e8">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_c8499d4b-97c7-4a5c-8996-2ee80a16257a">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_8">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_8_0_">
                          <gml:posList srsDimension="3">294094.934 5629758.302 207.959 294090.252 5629753.202 204.039 294097.045 5629746.95 204.04 294101.773 5629742.594 204.039 294106.463 5629747.679 207.959 294094.934 5629758.302 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_bfdd7d12-300a-4c37-927d-4212d51ae616">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_a0021243-7fe0-42cd-94ab-86cc92bacbb0">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_9">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_9_0_">
                          <gml:posList srsDimension="3">294106.463 5629747.679 207.959 294111.154 5629752.764 204.039 294099.615 5629763.402 204.039 294094.934 5629758.302 207.959 294106.463 5629747.679 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:GroundSurface gml:id="UUID_cc1e5643-6797-4102-8f72-4ba6b9bab448">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_7f7a3231-36bc-4d2d-b033-e414eedd8433">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179274_2_0">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179274_2_0_0_">
                          <gml:posList srsDimension="3">294097.045 5629746.95 190.192 294090.252 5629753.202 190.192 294094.934 5629758.302 190.192 294099.615 5629763.402 190.192 294111.154 5629752.764 190.192 294106.463 5629747.679 190.192 294101.773 5629742.594 190.192 294097.045 5629746.95 190.192</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:GroundSurface>
          </bldg:boundedBy>
<!--          <bldg:boundedBy>-->
<!--&lt;!&ndash;            <bldg:WallSurface gml:id="UUID_c2b6dfb6-784e-48f5-9e56-039087663b3b">&ndash;&gt;-->
<!--&lt;!&ndash;              <core:creationDate>2018-12-21</core:creationDate>&ndash;&gt;-->
<!--&lt;!&ndash;              <bldg:lod2MultiSurface>&ndash;&gt;-->
<!--&lt;!&ndash;                <gml:MultiSurface>&ndash;&gt;-->
<!--&lt;!&ndash;                  <gml:surfaceMember>&ndash;&gt;-->
<!--&lt;!&ndash;                    <gml:Polygon gml:id="UUID_f83f08e8-190b-44ba-ad59-26332670500f">&ndash;&gt;-->
<!--&lt;!&ndash;                      <gml:exterior>&ndash;&gt;-->
<!--&lt;!&ndash;                        <gml:LinearRing>&ndash;&gt;-->
<!--&lt;!&ndash;                          <gml:posList srsDimension="3">294094.934 5629758.302 207.959 294094.934 5629758.302 207.959 294094.934 5629758.302 190.192 294094.934 5629758.302 190.192 294094.934 5629758.302 207.959</gml:posList>&ndash;&gt;-->
<!--&lt;!&ndash;                        </gml:LinearRing>&ndash;&gt;-->
<!--&lt;!&ndash;                      </gml:exterior>&ndash;&gt;-->
<!--&lt;!&ndash;                    </gml:Polygon>&ndash;&gt;-->
<!--&lt;!&ndash;                  </gml:surfaceMember>&ndash;&gt;-->
<!--&lt;!&ndash;                </gml:MultiSurface>&ndash;&gt;-->
<!--&lt;!&ndash;              </bldg:lod2MultiSurface>&ndash;&gt;-->
<!--&lt;!&ndash;            </bldg:WallSurface>&ndash;&gt;-->
<!--          </bldg:boundedBy>-->
          <bldg:boundedBy>
            <bldg:ClosureSurface>
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_4985c59f-5582-4487-ad94-c04e63f033c9">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294090.252 5629753.202 204.039 294094.934 5629758.302 207.959 294094.934 5629758.302 190.192 294090.252 5629753.202 190.192 294090.252 5629753.202 193.302 294090.252 5629753.202 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:ClosureSurface>
          </bldg:boundedBy>
<!--          <bldg:boundedBy>-->
<!--            <bldg:WallSurface gml:id="UUID_8154cfb6-9ba9-451e-afe8-2335b9c2447b">-->
<!--              <core:creationDate>2018-12-21</core:creationDate>-->
<!--              <bldg:lod2MultiSurface>-->
<!--                <gml:MultiSurface>-->
<!--                  <gml:surfaceMember>-->
<!--                    <gml:Polygon gml:id="UUID_e079ae16-634e-449c-9653-37df687826a1">-->
<!--                      <gml:exterior>-->
<!--                        <gml:LinearRing>-->
<!--                          <gml:posList srsDimension="3">294094.934 5629758.302 207.959 294099.615 5629763.402 204.039 294094.934 5629758.302 207.959 294094.934 5629758.302 207.959</gml:posList>-->
<!--                        </gml:LinearRing>-->
<!--                      </gml:exterior>-->
<!--                    </gml:Polygon>-->
<!--                  </gml:surfaceMember>-->
<!--                </gml:MultiSurface>-->
<!--              </bldg:lod2MultiSurface>-->
<!--            </bldg:WallSurface>-->
<!--          </bldg:boundedBy>-->
          <bldg:boundedBy>
            <bldg:ClosureSurface>
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_e73a202a-8fa1-46e2-972f-6a5104b985bb">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294099.615 5629763.402 204.039 294099.615 5629763.402 190.192 294094.934 5629758.302 190.192 294094.934 5629758.302 207.959 294099.615 5629763.402 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:ClosureSurface>
          </bldg:boundedBy>
        </bldg:BuildingPart>
      </bldg:consistsOfBuildingPart>
      <bldg:consistsOfBuildingPart>
        <bldg:BuildingPart gml:id="GUID_faI66xtlEQ3Cj3FD">
          <core:creationDate>2018-12-21</core:creationDate>
          <gen:stringAttribute name="DatenquelleDachhoehe">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleBodenhoehe">
            <gen:value>1100</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleLage">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <bldg:roofType>3100</bldg:roofType>
          <bldg:measuredHeight uom="urn:adv:uom:m">18.577</bldg:measuredHeight>
          <bldg:lod2Solid>
            <gml:Solid>
              <gml:exterior>
                <gml:CompositeSurface>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_8"/>
                  <gml:surfaceMember xlink:href="#UUID_666c3b8d-8b24-4fdd-bb29-1579db3b7454"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_4"/>
                  <gml:surfaceMember xlink:href="#UUID_05820020-7e72-4aa4-9e85-940d7b818af4"/>
                  <gml:surfaceMember xlink:href="#UUID_40dbbad8-b64b-4636-ae8c-9035a76cdcfe"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_2"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_9"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_5"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_0"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_6"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7179273_2_3"/>
                  <gml:surfaceMember xlink:href="#UUID_e4866cf7-e22b-4615-8367-7125526e5825"/>
                  <gml:surfaceMember xlink:href="#UUID_8c73cf52-8c08-4560-b5e9-9cd1f2015cca"/>
                </gml:CompositeSurface>
              </gml:exterior>
            </gml:Solid>
          </bldg:lod2Solid>
          <bldg:lod2TerrainIntersection>
            <gml:MultiCurve>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294090.252 5629753.202 190.404 294090.811 5629753.811 190.302 294090.985 5629754.0 190.27 294091.0 5629754.017 190.269 294091.902 5629755.0 190.273 294092.0 5629755.106 190.268 294092.82 5629756.0 190.195 294093.0 5629756.196 190.192 294093.738 5629757.0 190.204 294094.0 5629757.285 190.223 294094.656 5629758.0 190.256 294094.933 5629758.302 190.27</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294087.367 5629755.861 189.381 294087.624 5629755.624 189.566 294088.0 5629755.278 189.857 294088.144 5629755.144 189.906 294088.301 5629755.0 189.985 294088.665 5629754.665 190.257 294089.0 5629754.356 190.276 294089.185 5629754.185 190.301 294089.386 5629754.0 190.305 294089.706 5629753.706 190.315 294090.0 5629753.434 190.357 294090.226 5629753.226 190.402 294090.252 5629753.202 190.404</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294087.367 5629755.861 189.381 294087.5 5629756.0 189.435 294087.597 5629756.102 189.467</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294087.597 5629756.102 189.467 294088.0 5629756.531 189.598 294088.441 5629757.0 189.635 294089.0 5629757.595 189.743 294089.38 5629758.0 189.8 294090.0 5629758.66 189.98 294090.32 5629759.0 190.025 294091.0 5629759.724 190.033 294091.259 5629760.0 190.031 294092.0 5629760.788 190.144 294092.112 5629760.908 190.161</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294092.112 5629760.908 190.161 294092.199 5629761.0 190.174 294093.0 5629761.853 190.21 294093.138 5629762.0 190.21 294094.0 5629762.917 190.279 294094.078 5629763.0 190.288 294095.0 5629763.982 190.39 294095.017 5629764.0 190.391 294095.287 5629764.287 190.424 294095.957 5629765.0 190.506 294096.0 5629765.046 190.512 294096.856 5629765.957 190.565</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294096.856 5629765.957 190.565 294096.908 5629765.908 190.565 294097.0 5629765.824 190.566 294097.428 5629765.428 190.537 294097.889 5629765.0 190.497 294097.947 5629764.947 190.489 294098.0 5629764.898 190.492 294098.466 5629764.466 190.501 294098.969 5629764.0 190.51 294098.985 5629763.985 190.509 294099.0 5629763.972 190.51 294099.504 5629763.504 190.52 294099.615 5629763.402 190.529</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294094.933 5629758.302 190.27 294095.0 5629758.374 190.274 294095.574 5629759.0 190.297 294096.0 5629759.464 190.319 294096.492 5629760.0 190.34 294097.0 5629760.553 190.361 294097.41 5629761.0 190.382 294098.0 5629761.643 190.419 294098.328 5629762.0 190.446 294099.0 5629762.732 190.509 294099.246 5629763.0 190.53 294099.615 5629763.402 190.529</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:MultiCurve>
          </bldg:lod2TerrainIntersection>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_228be0c8-a474-4187-b132-280e92dce697">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_835804f7-2af9-414d-bd4e-1b612135a901">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_2">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_2_0_">
                          <gml:posList srsDimension="3">294090.252 5629753.202 204.039 294087.367 5629755.861 204.039 294087.367 5629755.861 189.381 294090.252 5629753.202 189.381 294090.252 5629753.202 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_41b7d4b8-716b-4d4f-8747-23719d4f0e28">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_26980e2b-a665-40ad-a3b8-51eacf924ee4">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_3">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_3_0_">
                          <gml:posList srsDimension="3">294087.367 5629755.861 204.039 294087.597 5629756.102 204.228 294087.597 5629756.102 189.381 294087.367 5629755.861 189.381 294087.367 5629755.861 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_d76c65b0-2620-4191-b94d-59ea5afc785b">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_981f1140-926a-44c8-b135-4de36d1c1067">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_4">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_4_0_">
                          <gml:posList srsDimension="3">294087.597 5629756.102 204.228 294092.112 5629760.908 207.959 294092.112 5629760.908 189.381 294087.597 5629756.102 189.381 294087.597 5629756.102 204.228</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_a79f9b89-e3c2-4cbc-aa48-fe0611ef8ad5">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_4335dc01-077c-4149-aff7-e404ae8060a8">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_5">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_5_0_">
                          <gml:posList srsDimension="3">294092.112 5629760.908 207.959 294096.856 5629765.957 204.039 294096.856 5629765.957 189.381 294092.112 5629760.908 189.381 294092.112 5629760.908 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_14dec93a-91a2-49c4-852f-cd377fe305aa">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_344fe4b4-0ffd-4fbf-81d5-35bb4f0ded43">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_6">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_6_0_">
                          <gml:posList srsDimension="3">294096.856 5629765.957 204.039 294099.615 5629763.402 204.039 294099.615 5629763.402 189.381 294096.856 5629765.957 189.381 294096.856 5629765.957 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_ffb13006-b4a4-4efe-81e4-c65a572cffff">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_eb81a4c0-637c-4f8c-8885-f6c45e62500f">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_8">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_8_0_">
                          <gml:posList srsDimension="3">294092.112 5629760.908 207.959 294087.597 5629756.102 204.228 294087.367 5629755.861 204.039 294090.252 5629753.202 204.039 294094.933 5629758.302 207.959 294092.112 5629760.908 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_3ddbb534-cc99-4a9a-8c62-ce38cdabf2a9">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_2d366e2f-16b4-4e86-b28a-b7e69d9f3a42">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_9">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_9_0_">
                          <gml:posList srsDimension="3">294094.933 5629758.302 207.959 294099.615 5629763.402 204.039 294096.856 5629765.957 204.039 294092.112 5629760.908 207.959 294094.933 5629758.302 207.959</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:GroundSurface gml:id="UUID_75e6e46a-0545-4f5d-90ce-dabb37d7bcec">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_ff478eeb-78a5-4e52-8461-f4138962bb76">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7179273_2_0">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7179273_2_0_0_">
                          <gml:posList srsDimension="3">294094.933 5629758.302 189.381 294090.252 5629753.202 189.381 294087.367 5629755.861 189.381 294087.597 5629756.102 189.381 294092.112 5629760.908 189.381 294096.856 5629765.957 189.381 294099.615 5629763.402 189.381 294094.933 5629758.302 189.381</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:GroundSurface>
          </bldg:boundedBy>
<!--          <bldg:boundedBy>-->
<!--            <bldg:WallSurface gml:id="UUID_848e3f73-bfdd-46f6-bf72-33141ac1e141">-->
<!--              <core:creationDate>2018-12-21</core:creationDate>-->
<!--              <bldg:lod2MultiSurface>-->
<!--                <gml:MultiSurface>-->
<!--&lt;!&ndash;                  <gml:surfaceMember>&ndash;&gt;-->
<!--&lt;!&ndash;                    <gml:Polygon gml:id="UUID_05820020-7e72-4aa4-9e85-940d7b818af4">&ndash;&gt;-->
<!--&lt;!&ndash;                      <gml:exterior>&ndash;&gt;-->
<!--&lt;!&ndash;                        <gml:LinearRing>&ndash;&gt;-->
<!--&lt;!&ndash;                          <gml:posList srsDimension="3">294094.933 5629758.302 207.959 294090.252 5629753.202 204.039 294094.933 5629758.302 207.959 294094.933 5629758.302 207.959</gml:posList>&ndash;&gt;-->
<!--&lt;!&ndash;                        </gml:LinearRing>&ndash;&gt;-->
<!--&lt;!&ndash;                      </gml:exterior>&ndash;&gt;-->
<!--&lt;!&ndash;                    </gml:Polygon>&ndash;&gt;-->
<!--&lt;!&ndash;                  </gml:surfaceMember>&ndash;&gt;-->
<!--                  <gml:surfaceMember>-->
<!--                    <gml:Polygon gml:id="UUID_e4866cf7-e22b-4615-8367-7125526e5825">-->
<!--                      <gml:exterior>-->
<!--                        <gml:LinearRing>-->
<!--                          <gml:posList srsDimension="3">294090.252 5629753.202 190.192 294090.252 5629753.202 189.381 294094.933 5629758.302 189.381 294094.933 5629758.302 190.192 294090.252 5629753.202 190.192</gml:posList>-->
<!--                        </gml:LinearRing>-->
<!--                      </gml:exterior>-->
<!--                    </gml:Polygon>-->
<!--                  </gml:surfaceMember>-->
<!--                </gml:MultiSurface>-->
<!--              </bldg:lod2MultiSurface>-->
<!--            </bldg:WallSurface>-->
<!--          </bldg:boundedBy>-->
          <bldg:boundedBy>
            <bldg:ClosureSurface>
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_8c73cf52-8c08-4560-b5e9-9cd1f2015cca">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294090.252 5629753.202 204.039 294090.252 5629753.202 190.192 294094.933 5629758.302 190.192 294094.933 5629758.302 207.959 294090.252 5629753.202 204.039</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:ClosureSurface>
          </bldg:boundedBy>
<!--          <bldg:boundedBy>-->
<!--            <bldg:WallSurface gml:id="UUID_56c12f55-d891-4f29-bc7e-d587a856738b">-->
<!--              <core:creationDate>2018-12-21</core:creationDate>-->
<!--              <bldg:lod2MultiSurface>-->
<!--                <gml:MultiSurface>-->
<!--                  <gml:surfaceMember>-->
<!--                    <gml:Polygon gml:id="UUID_666c3b8d-8b24-4fdd-bb29-1579db3b7454">-->
<!--                      <gml:exterior>-->
<!--                        <gml:LinearRing>-->
<!--                          <gml:posList srsDimension="3">294094.933 5629758.302 207.959 294094.933 5629758.302 207.959 294094.933 5629758.302 189.381 294099.615 5629763.402 189.381 294099.615 5629763.402 190.192 294094.933 5629758.302 190.192 294094.933 5629758.302 207.959</gml:posList>-->
<!--                        </gml:LinearRing>-->
<!--                      </gml:exterior>-->
<!--                    </gml:Polygon>-->
<!--                  </gml:surfaceMember>-->
<!--                </gml:MultiSurface>-->
<!--              </bldg:lod2MultiSurface>-->
<!--            </bldg:WallSurface>-->
<!--          </bldg:boundedBy>-->
<!--          <bldg:boundedBy>-->
<!--            <bldg:ClosureSurface>-->
<!--              <core:creationDate>2018-12-21</core:creationDate>-->
<!--              <bldg:lod2MultiSurface>-->
<!--                <gml:MultiSurface>-->
<!--                  <gml:surfaceMember>-->
<!--                    <gml:Polygon gml:id="UUID_40dbbad8-b64b-4636-ae8c-9035a76cdcfe">-->
<!--                      <gml:exterior>-->
<!--                        <gml:LinearRing>-->
<!--                          <gml:posList srsDimension="3">294099.615 5629763.402 204.039 294094.933 5629758.302 207.959 294094.933 5629758.302 190.192 294099.615 5629763.402 190.192 294099.615 5629763.402 204.039</gml:posList>-->
<!--                        </gml:LinearRing>-->
<!--                      </gml:exterior>-->
<!--                    </gml:Polygon>-->
<!--                  </gml:surfaceMember>-->
<!--                </gml:MultiSurface>-->
<!--              </bldg:lod2MultiSurface>-->
<!--            </bldg:ClosureSurface>-->
<!--          </bldg:boundedBy>-->
          <bldg:address>
            <core:Address gml:id="UUID_e3fe691d-3b4d-4f42-918c-29248d3dfde1">
              <core:xalAddress>
                <xal:AddressDetails>
                  <xal:Country>
                    <xal:CountryName>Germany</xal:CountryName>
                    <xal:Locality Type="Town">
                      <xal:LocalityName>Aachen</xal:LocalityName>
                      <xal:Thoroughfare Type="Street">
                        <xal:ThoroughfareNumber>5</xal:ThoroughfareNumber>
                        <xal:ThoroughfareName>Lousbergstraße</xal:ThoroughfareName>
                      </xal:Thoroughfare>
                      <xal:PostalCode>
                        <xal:PostalCodeNumber>52072</xal:PostalCodeNumber>
                      </xal:PostalCode>
                    </xal:Locality>
                  </xal:Country>
                </xal:AddressDetails>
              </core:xalAddress>
            </core:Address>
          </bldg:address>
        </bldg:BuildingPart>
      </bldg:consistsOfBuildingPart>
      <bldg:address>
        <core:Address gml:id="UUID_1f1a2b00-1162-46db-8346-411702c78e4b">
          <core:xalAddress>
            <xal:AddressDetails>
              <xal:Country>
                <xal:CountryName>Germany</xal:CountryName>
                <xal:Locality Type="Town">
                  <xal:LocalityName>Aachen</xal:LocalityName>
                  <xal:Thoroughfare Type="Street">
                    <xal:ThoroughfareNumber>5</xal:ThoroughfareNumber>
                    <xal:ThoroughfareName>Lousbergstraße</xal:ThoroughfareName>
                  </xal:Thoroughfare>
                  <xal:PostalCode>
                    <xal:PostalCodeNumber>52072</xal:PostalCodeNumber>
                  </xal:PostalCode>
                </xal:Locality>
              </xal:Country>
            </xal:AddressDetails>
          </core:xalAddress>
        </core:Address>
      </bldg:address>
    </bldg:Building>
  </core:cityObjectMember>
  <core:cityObjectMember>
    <bldg:Building gml:id="DENW39AL1000MjeA">
      <core:creationDate>2018-12-21</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
        <core:externalObject>
          <core:name>DENW39AL1000MjeA</core:name>
        </core:externalObject>
      </core:externalReference>
      <gen:stringAttribute name="Gemeindeschluessel">
        <gen:value>05334002</gen:value>
      </gen:stringAttribute>
      <bldg:function>31001_1010</bldg:function>
      <bldg:yearOfConstruction>2002</bldg:yearOfConstruction>
      <bldg:consistsOfBuildingPart>
        <bldg:BuildingPart gml:id="UUID_c12594dd-72fb-47a1-b174-a8335231f76a">
          <core:creationDate>2018-12-21</core:creationDate>
          <gen:stringAttribute name="DatenquelleDachhoehe">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleBodenhoehe">
            <gen:value>1100</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleLage">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <bldg:roofType>3100</bldg:roofType>
          <bldg:measuredHeight uom="urn:adv:uom:m">16.147</bldg:measuredHeight>
          <bldg:lod2Solid>
            <gml:Solid>
              <gml:exterior>
                <gml:CompositeSurface>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_4"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_19"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_0"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_5"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_14"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_15"/>
                  <gml:surfaceMember xlink:href="#UUID_262f0163-8c10-4559-9712-adeb8912bc52"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_9"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_32"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_22"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_12"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_25"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_3"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_29"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_30"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_1"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_26"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_13"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_27"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_23"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_24"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_21"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_28"/>
                  <gml:surfaceMember xlink:href="#UUID_ccc2ca06-c08a-40af-8cf6-bb3fadf3f708"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_31"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_20"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_7"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_18"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_17"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_11"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_10"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_2"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_8"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180441_2_16"/>
                </gml:CompositeSurface>
              </gml:exterior>
            </gml:Solid>
          </bldg:lod2Solid>
          <bldg:lod2TerrainIntersection>
            <gml:MultiCurve>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294807.755 5629951.359 199.198 294807.844 5629951.0 199.211 294807.876 5629950.876 199.199 294808.0 5629950.376 198.804 294808.075 5629950.075 198.593 294808.094 5629950.0 198.596 294808.254 5629949.357 198.271</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294807.755 5629951.359 199.198 294808.0 5629951.421 199.11 294808.563 5629951.563 198.946 294808.746 5629951.609 198.856</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294808.186 5629953.854 199.002 294808.319 5629953.319 199.004 294808.399 5629953.0 198.976 294808.519 5629952.519 198.864 294808.648 5629952.0 198.871 294808.719 5629951.719 198.887 294808.746 5629951.609 198.856</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294807.195 5629953.604 199.285 294807.742 5629953.742 199.148 294808.0 5629953.807 199.079 294808.186 5629953.854 199.002</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294806.558 5629956.138 198.439 294806.592 5629956.0 198.476 294806.674 5629955.674 198.712 294806.844 5629955.0 199.023 294806.875 5629954.875 199.057 294807.0 5629954.379 199.174 294807.076 5629954.076 199.24 294807.095 5629954.0 199.259 294807.195 5629953.604 199.285</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294806.558 5629956.138 198.439 294807.0 5629956.25 198.438 294807.335 5629956.335 198.48 294808.0 5629956.503 198.52 294808.673 5629956.673 198.479 294809.0 5629956.756 198.43 294809.967 5629957.0 198.304 294810.0 5629957.008 198.3 294810.011 5629957.011 198.295 294811.0 5629957.261 197.894 294811.35 5629957.35 197.787 294811.514 5629957.391 197.741</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294811.514 5629957.391 197.741 294811.553 5629957.401 197.73</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294811.553 5629957.401 197.73 294812.0 5629957.514 197.603 294812.688 5629957.688 197.546 294813.0 5629957.767 197.513 294813.922 5629958.0 197.472 294814.0 5629958.02 197.472 294814.026 5629958.026 197.471 294815.0 5629958.272 197.464 294815.365 5629958.365 197.432 294815.477 5629958.393 197.422</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294815.477 5629958.393 197.422 294815.575 5629958.0 197.392 294815.66 5629957.66 197.415 294815.825 5629957.0 197.639 294815.86 5629956.86 197.677 294816.0 5629956.3 197.772 294816.06 5629956.06 197.804 294816.075 5629956.0 197.816 294816.26 5629955.26 197.908 294816.325 5629955.0 197.937 294816.46 5629954.46 197.966 294816.575 5629954.0 197.961 294816.66 5629953.66 197.975 294816.824 5629953.0 198.001 294816.859 5629952.859 198.002 294817.0 5629952.297 198.001 294817.059 5629952.059 198.005 294817.074 5629952.0 198.008 294817.259 5629951.259 198.061 294817.324 5629951.0 198.071 294817.459 5629950.459 198.1 294817.574 5629950.0 198.137 294817.659 5629949.659 198.161 294817.824 5629949.0 198.191 294817.859 5629948.859 198.201 294818.0 5629948.294 198.265 294818.059 5629948.059 198.287 294818.073 5629948.0 198.292 294818.259 5629947.259 198.338 294818.323 5629947.0 198.354 294818.459 5629946.459 198.426 294818.573 5629946.0 198.533 294818.658 5629945.658 198.558 294818.823 5629945.0 198.704 294818.858 5629944.858 198.74 294819.0 5629944.291 198.89 294819.058 5629944.058 198.954 294819.073 5629944.0 198.961 294819.163 5629943.639 199.003</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294818.971 5629943.597 199.007 294819.0 5629943.603 199.004 294819.163 5629943.639 199.003</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294818.75 5629943.173 199.076 294818.971 5629943.597 199.007</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294818.45 5629942.801 199.175 294818.61 5629943.0 199.109 294818.75 5629943.173 199.076</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294818.081 5629942.497 199.288 294818.45 5629942.801 199.175</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294817.658 5629942.273 199.379 294818.0 5629942.454 199.306 294818.081 5629942.497 199.288</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294817.2 5629942.137 199.45 294817.658 5629942.273 199.379</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294816.767 5629942.086 199.493 294817.0 5629942.113 199.466 294817.129 5629942.129 199.458 294817.2 5629942.137 199.45</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294816.333 5629942.112 199.52 294816.767 5629942.086 199.493</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294815.909 5629942.214 199.519 294816.0 5629942.192 199.52 294816.155 5629942.155 199.521 294816.333 5629942.112 199.52</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294815.51 5629942.389 199.489 294815.909 5629942.214 199.519</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294815.218 5629942.584 199.493 294815.437 5629942.437 199.478 294815.51 5629942.389 199.489</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294815.148 5629942.631 199.498 294815.218 5629942.584 199.493</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294814.471 5629942.468 199.566 294815.0 5629942.595 199.533 294815.148 5629942.631 199.498</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294814.471 5629942.468 199.566 294814.586 5629942.0 199.648 294814.668 5629941.668 199.683 294814.677 5629941.629 199.684</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294811.442 5629940.82 199.967 294811.946 5629940.946 199.933 294812.0 5629940.96 199.932 294812.162 5629941.0 199.912 294813.0 5629941.21 199.799 294813.28 5629941.28 199.775 294814.0 5629941.46 199.719 294814.613 5629941.613 199.69 294814.677 5629941.629 199.684</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294811.236 5629941.659 199.935 294811.319 5629941.319 199.952 294811.398 5629941.0 199.972 294811.442 5629940.82 199.967</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294810.245 5629941.409 200.003 294810.464 5629941.464 199.994 294811.0 5629941.599 199.958 294811.236 5629941.659 199.935</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294808.824 5629947.102 198.258 294808.849 5629947.0 198.308 294808.88 5629946.88 198.37 294809.0 5629946.397 198.74 294809.079 5629946.079 199.003 294809.099 5629946.0 199.032 294809.279 5629945.279 199.382 294809.349 5629945.0 199.454 294809.479 5629944.479 199.465 294809.598 5629944.0 199.462 294809.679 5629943.679 199.4 294809.848 5629943.0 199.369 294809.878 5629942.878 199.425 294810.0 5629942.391 199.713 294810.078 5629942.078 199.906 294810.097 5629942.0 199.939 294810.245 5629941.409 200.003</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294808.824 5629947.102 198.258 294809.0 5629947.146 198.227 294809.196 5629947.196 198.296 294809.815 5629947.352 198.474</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294809.245 5629949.597 198.346 294809.316 5629949.316 198.152 294809.397 5629949.0 198.192 294809.519 5629948.519 198.148 294809.65 5629948.0 198.112 294809.721 5629947.721 198.286 294809.815 5629947.352 198.474</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294808.254 5629949.357 198.271 294808.39 5629949.39 198.296 294809.0 5629949.538 198.354 294809.245 5629949.597 198.346</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:MultiCurve>
          </bldg:lod2TerrainIntersection>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_684aeadd-668a-4682-b19e-0091a56d1ce7">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_9455e0e2-3f69-487c-8e84-4c9d0739bc17">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_1">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_1_0_">
                          <gml:posList srsDimension="3">294808.254 5629949.357 207.337 294807.755 5629951.359 207.34 294807.755 5629951.359 197.392 294808.254 5629949.357 197.392 294808.254 5629949.357 207.337</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_5dbb2338-1676-4bb5-85eb-512c6643ba95">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_67ebc0f0-312b-49ac-a3c0-d513d889692f">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_2">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_2_0_">
                          <gml:posList srsDimension="3">294807.755 5629951.359 207.34 294808.746 5629951.609 208.58 294808.746 5629951.609 197.392 294807.755 5629951.359 197.392 294807.755 5629951.359 207.34</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_298a0740-a5cb-4488-80a5-42567a84e47b">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_3831680d-14f1-48c2-a36c-2850e2dc4ba6">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_3">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_3_0_">
                          <gml:posList srsDimension="3">294808.746 5629951.609 208.58 294808.186 5629953.854 208.583 294808.186 5629953.854 197.392 294808.746 5629951.609 197.392 294808.746 5629951.609 208.58</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_cd01b2a8-2e70-4334-ac9d-eef3ca6f653d">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_d7a59591-fee9-4b8a-a221-62bec9e8afa7">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_4">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_4_0_">
                          <gml:posList srsDimension="3">294808.186 5629953.854 208.583 294807.195 5629953.604 207.342 294807.195 5629953.604 197.392 294808.186 5629953.854 197.392 294808.186 5629953.854 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_6cfa5a5b-66dd-4bf6-95eb-aa13d723019f">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_1e37574d-d153-45cc-858b-6590d9290602">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_5">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_5_0_">
                          <gml:posList srsDimension="3">294807.195 5629953.604 207.342 294806.558 5629956.138 207.339 294806.558 5629956.138 197.392 294807.195 5629953.604 197.392 294807.195 5629953.604 207.342</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_abd9812f-eff1-4d0d-9f00-38dea99a831d">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_27c4be0c-9931-48b6-b470-0d3f337214ac">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_7">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_7_0_">
                          <gml:posList srsDimension="3">294811.514 5629957.391 213.539 294811.553 5629957.401 213.49 294811.553 5629957.401 197.392 294811.514 5629957.391 197.392 294811.514 5629957.391 213.539</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_40ad0650-defa-43d7-8af4-97f86237110d">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_1b3ccf0e-5b08-4169-bef9-26772de37160">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_8">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_8_0_">
                          <gml:posList srsDimension="3">294811.553 5629957.401 213.49 294815.477 5629958.393 208.583 294815.477 5629958.393 197.392 294811.553 5629957.401 197.392 294811.553 5629957.401 213.49</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_2ebccfd6-d059-4cfd-933d-6730eb9145f2">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_17314586-d364-499a-8cfa-2a6c36732bbf">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_9">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_9_0_">
                          <gml:posList srsDimension="3">294815.477 5629958.393 208.583 294819.163 5629943.639 208.583 294819.163 5629943.639 197.392 294815.477 5629958.393 197.392 294815.477 5629958.393 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_62bbf588-9e09-481a-a041-e9bd95a5c453">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_da06a8cb-4398-4e99-8455-9b3992077419">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_10">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_10_0_">
                          <gml:posList srsDimension="3">294819.163 5629943.639 208.583 294818.971 5629943.597 208.822 294818.971 5629943.597 197.392 294819.163 5629943.639 197.392 294819.163 5629943.639 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_19e5bb45-bb4a-42a2-a0fd-21340b6e83e9">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_7264f39a-0f46-425a-a9ff-7e9f67f4663d">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_11">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_11_0_">
                          <gml:posList srsDimension="3">294818.971 5629943.597 208.822 294818.75 5629943.173 209.207 294818.75 5629943.173 197.392 294818.971 5629943.597 197.392 294818.971 5629943.597 208.822</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_0bf89aef-7ade-4e96-938e-4474394d98de">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_d9fdef85-4f42-4f7a-bc9a-13268c6de7f4">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_12">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_12_0_">
                          <gml:posList srsDimension="3">294818.75 5629943.173 209.207 294818.45 5629942.801 209.67 294818.45 5629942.801 197.392 294818.75 5629943.173 197.392 294818.75 5629943.173 209.207</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_fe123a49-aeaa-4fd3-84b2-c7c679ec788f">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_6f05dd84-ede5-455f-91a7-ed3e0a67e7ed">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_13">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_13_0_">
                          <gml:posList srsDimension="3">294818.45 5629942.801 209.67 294818.081 5629942.497 210.194 294818.081 5629942.497 197.392 294818.45 5629942.801 197.392 294818.45 5629942.801 209.67</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_ff4573cd-461b-41f8-9ff0-73dc8f50a8ca">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_15bb9a9f-49ab-4432-be17-d1beeab9524d">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_14">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_14_0_">
                          <gml:posList srsDimension="3">294818.081 5629942.497 210.194 294817.658 5629942.273 210.758 294817.658 5629942.273 197.392 294818.081 5629942.497 197.392 294818.081 5629942.497 210.194</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_ac77c3bd-8741-40b7-8d53-27a18defc980">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_5a0eaa57-3cf7-44b2-a116-87f8af9ce951">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_15">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_15_0_">
                          <gml:posList srsDimension="3">294817.658 5629942.273 210.758 294817.2 5629942.137 211.337 294817.2 5629942.137 197.392 294817.658 5629942.273 197.392 294817.658 5629942.273 210.758</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_e775cc4b-d613-48c6-90a7-d172c6979918">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_589b2969-25e8-4cd4-b27b-d9342b95d886">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_16">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_16_0_">
                          <gml:posList srsDimension="3">294817.2 5629942.137 211.337 294816.767 5629942.086 211.862 294816.767 5629942.086 197.392 294817.2 5629942.137 197.392 294817.2 5629942.137 211.337</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_7ead41a4-8856-4f0a-85ff-0f41afaaeb7b">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_17921e9a-95ef-48cb-ae70-11f47494c3ab">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_17">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_17_0_">
                          <gml:posList srsDimension="3">294816.767 5629942.086 211.862 294816.333 5629942.112 212.366 294816.333 5629942.112 197.392 294816.767 5629942.086 197.392 294816.767 5629942.086 211.862</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_bf719941-2941-43ca-a872-d51cef0b5b88">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_89a469d4-8c09-4641-bc75-e0e1fd0a79b6">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_18">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_18_0_">
                          <gml:posList srsDimension="3">294816.333 5629942.112 212.366 294815.909 5629942.214 212.835 294815.909 5629942.214 197.392 294816.333 5629942.112 197.392 294816.333 5629942.112 212.366</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_5b589f5e-59d7-4f15-ae5c-88655bd8f10e">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_7a8da13c-4ce9-44c5-8e0c-1200703198f5">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_19">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_19_0_">
                          <gml:posList srsDimension="3">294815.909 5629942.214 212.835 294815.51 5629942.389 213.253 294815.51 5629942.389 197.392 294815.909 5629942.214 197.392 294815.909 5629942.214 212.835</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_e1645198-3517-4a33-9f8e-0c03a8b252ae">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_95762100-7a9c-45ca-b10e-41cc718dd10b">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_20">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_20_0_">
                          <gml:posList srsDimension="3">294815.51 5629942.389 213.253 294815.218 5629942.584 213.539 294815.218 5629942.584 197.392 294815.51 5629942.389 197.392 294815.51 5629942.389 213.253</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_525f76a8-bd4d-4010-bda6-8233d957329c">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_596bd25c-4748-4da9-a0ca-ff32fd694577">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_21">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_21_0_">
                          <gml:posList srsDimension="3">294815.218 5629942.584 213.539 294815.148 5629942.631 213.47 294815.148 5629942.631 197.392 294815.218 5629942.584 197.392 294815.218 5629942.584 213.539</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_c58b5b07-8ae1-409e-b545-588a5688501c">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_42a5067f-6a2c-4288-b284-b2ebed6fd09e">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_22">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_22_0_">
                          <gml:posList srsDimension="3">294815.148 5629942.631 213.47 294814.471 5629942.468 212.625 294814.471 5629942.468 197.392 294815.148 5629942.631 197.392 294815.148 5629942.631 213.47</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_a7eb2c90-5fea-4699-83b8-3fc2ac5bb9b3">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_43e41e24-70b8-4547-8954-ac1b98bc94bb">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_23">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_23_0_">
                          <gml:posList srsDimension="3">294814.471 5629942.468 212.625 294814.677 5629941.629 212.62 294814.677 5629941.629 197.392 294814.471 5629942.468 197.392 294814.471 5629942.468 212.625</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_7c5ac7da-9469-445a-b5ba-30fb4ef0c1c5">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_b64f23be-a68e-4852-962c-2b646de93607">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_24">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_24_0_">
                          <gml:posList srsDimension="3">294814.677 5629941.629 212.62 294811.442 5629940.82 208.572 294811.442 5629940.82 197.392 294814.677 5629941.629 197.392 294814.677 5629941.629 212.62</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_00c664c5-94e0-4e92-8629-b788f71912b3">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_93cd6f47-e617-46b3-b7c9-477a716aed74">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_25">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_25_0_">
                          <gml:posList srsDimension="3">294811.442 5629940.82 208.572 294811.236 5629941.659 208.577 294811.236 5629941.659 197.392 294811.442 5629940.82 197.392 294811.442 5629940.82 208.572</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_b97b95ac-a974-4053-9c93-ca4afeeb4816">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_beba79ae-8904-4828-a7ca-430b74b8a168">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_26">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_26_0_">
                          <gml:posList srsDimension="3">294811.236 5629941.659 208.577 294810.245 5629941.409 207.336 294810.245 5629941.409 197.392 294811.236 5629941.659 197.392 294811.236 5629941.659 208.577</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_ccb272f1-0985-4ddf-a585-0da9cbcd74c8">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_806acb6d-9084-464d-beee-a14e3ba7fcc9">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_27">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_27_0_">
                          <gml:posList srsDimension="3">294810.245 5629941.409 207.336 294808.824 5629947.102 207.342 294808.824 5629947.102 197.392 294810.245 5629941.409 197.392 294810.245 5629941.409 207.336</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_2b5fdc21-762d-4341-a4ab-fca3fb8f5295">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_9cc31a61-e26f-4596-bfb6-747ced0793f6">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_28">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_28_0_">
                          <gml:posList srsDimension="3">294808.824 5629947.102 207.342 294809.815 5629947.352 208.583 294809.815 5629947.352 197.392 294808.824 5629947.102 197.392 294808.824 5629947.102 207.342</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_c4571485-cf3e-4e2c-8305-f06e0947e601">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_90e638f9-7f61-465f-8bc4-1e11c64cf712">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_29">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_29_0_">
                          <gml:posList srsDimension="3">294809.815 5629947.352 208.583 294809.245 5629949.597 208.574 294809.245 5629949.597 197.392 294809.815 5629947.352 197.392 294809.815 5629947.352 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_5747f178-66c9-4627-85b3-b9a415b54f1e">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_50feeb06-fbd5-43f9-88ff-00655ba86199">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_30">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_30_0_">
                          <gml:posList srsDimension="3">294809.245 5629949.597 208.574 294808.254 5629949.357 207.337 294808.254 5629949.357 197.392 294809.245 5629949.597 197.392 294809.245 5629949.597 208.574</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_b836d3bc-c746-44a0-93f1-8bcf384fdcfc">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_de88c176-06d8-4512-99c2-96b8068bdadb">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_31">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_31_0_">
                          <gml:posList srsDimension="3">294811.514 5629957.391 213.539 294806.558 5629956.138 207.339 294807.195 5629953.604 207.342 294808.186 5629953.854 208.583 294808.746 5629951.609 208.58 294807.755 5629951.359 207.34 294808.254 5629949.357 207.337 294809.245 5629949.597 208.574 294809.815 5629947.352 208.583 294808.824 5629947.102 207.342 294810.245 5629941.409 207.336 294811.236 5629941.659 208.577 294811.442 5629940.82 208.572 294814.677 5629941.629 212.62 294814.471 5629942.468 212.625 294815.148 5629942.631 213.47 294815.218 5629942.584 213.539 294811.514 5629957.391 213.539</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_9a73f8b4-c6e6-43f0-8260-ce8cca55ea0d">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_8ee218a5-99db-427b-a440-f833466ce1bd">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_32">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_32_0_">
                          <gml:posList srsDimension="3">294815.218 5629942.584 213.539 294815.51 5629942.389 213.253 294815.909 5629942.214 212.835 294816.333 5629942.112 212.366 294816.767 5629942.086 211.862 294817.2 5629942.137 211.337 294817.658 5629942.273 210.758 294818.081 5629942.497 210.194 294818.45 5629942.801 209.67 294818.75 5629943.173 209.207 294818.971 5629943.597 208.822 294819.163 5629943.639 208.583 294815.477 5629958.393 208.583 294811.553 5629957.401 213.49 294811.514 5629957.391 213.539 294815.218 5629942.584 213.539</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:GroundSurface gml:id="UUID_2bedfb77-0acd-4d4c-94d2-736153934d7e">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_2ab6c396-a9fa-4103-8e31-2c75f5019147">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180441_2_0">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180441_2_0_0_">
                          <gml:posList srsDimension="3">294808.254 5629949.357 197.392 294807.755 5629951.359 197.392 294808.746 5629951.609 197.392 294808.186 5629953.854 197.392 294807.195 5629953.604 197.392 294806.558 5629956.138 197.392 294811.514 5629957.391 197.392 294811.553 5629957.401 197.392 294815.477 5629958.393 197.392 294819.163 5629943.639 197.392 294818.971 5629943.597 197.392 294818.75 5629943.173 197.392 294818.45 5629942.801 197.392 294818.081 5629942.497 197.392 294817.658 5629942.273 197.392 294817.2 5629942.137 197.392 294816.767 5629942.086 197.392 294816.333 5629942.112 197.392 294815.909 5629942.214 197.392 294815.51 5629942.389 197.392 294815.218 5629942.584 197.392 294815.148 5629942.631 197.392 294814.471 5629942.468 197.392 294814.677 5629941.629 197.392 294811.442 5629940.82 197.392 294811.236 5629941.659 197.392 294810.245 5629941.409 197.392 294808.824 5629947.102 197.392 294809.815 5629947.352 197.392 294809.245 5629949.597 197.392 294808.254 5629949.357 197.392</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:GroundSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_c103c6b3-98e9-470f-85d0-93b02df2dd89">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_262f0163-8c10-4559-9712-adeb8912bc52">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294807.552 5629956.389 208.583 294811.514 5629957.391 213.539 294811.514 5629957.391 208.583 294807.552 5629956.389 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:ClosureSurface>
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_ccc2ca06-c08a-40af-8cf6-bb3fadf3f708">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294806.558 5629956.138 207.339 294807.552 5629956.389 208.583 294811.514 5629957.391 208.583 294811.514 5629957.391 197.392 294806.558 5629956.138 197.392 294806.558 5629956.138 207.339</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:ClosureSurface>
          </bldg:boundedBy>
        </bldg:BuildingPart>
      </bldg:consistsOfBuildingPart>
      <bldg:consistsOfBuildingPart>
        <bldg:BuildingPart gml:id="GUID_1543405942858_7180440">
          <core:creationDate>2018-12-21</core:creationDate>
          <gen:stringAttribute name="DatenquelleDachhoehe">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleBodenhoehe">
            <gen:value>1100</gen:value>
          </gen:stringAttribute>
          <gen:stringAttribute name="DatenquelleLage">
            <gen:value>1000</gen:value>
          </gen:stringAttribute>
          <bldg:roofType>1000</bldg:roofType>
          <bldg:measuredHeight uom="urn:adv:uom:m">11.191</bldg:measuredHeight>
          <bldg:lod2Solid>
            <gml:Solid>
              <gml:exterior>
                <gml:CompositeSurface>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_3"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_2"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_0"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_5"/>
                  <gml:surfaceMember xlink:href="#UUID_87ea3ad8-ead8-4803-8278-a70882d4f78c"/>
                  <gml:surfaceMember xlink:href="#UUID_f05036fc-7c20-41b1-ac88-16d8dc994df2"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_4"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_7"/>
                  <gml:surfaceMember xlink:href="#GUID_1543405942858_7180440_2_6"/>
                  <gml:surfaceMember xlink:href="#UUID_852b6b99-8c92-48bf-953e-5061dec20caa"/>
                </gml:CompositeSurface>
              </gml:exterior>
            </gml:Solid>
          </bldg:lod2Solid>
          <bldg:lod2TerrainIntersection>
            <gml:MultiCurve>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294806.558 5629956.138 198.439 294807.0 5629956.25 198.438 294807.335 5629956.335 198.48 294808.0 5629956.503 198.52 294808.673 5629956.673 198.479 294809.0 5629956.756 198.43 294809.967 5629957.0 198.304 294810.0 5629957.008 198.3 294810.011 5629957.011 198.295 294811.0 5629957.261 197.894 294811.35 5629957.35 197.787 294811.553 5629957.401 197.73</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294805.773 5629959.257 199.0 294805.838 5629959.0 198.966 294805.87 5629958.87 198.928 294806.0 5629958.355 198.715 294806.071 5629958.071 198.586 294806.089 5629958.0 198.577 294806.272 5629957.272 198.142 294806.341 5629957.0 198.079 294806.473 5629956.473 198.35 294806.558 5629956.138 198.439</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294805.773 5629959.257 199.0 294806.0 5629959.314 198.998 294806.421 5629959.421 198.964 294806.966 5629959.559 198.848</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294806.612 5629960.955 198.992 294806.681 5629960.681 198.978 294806.854 5629960.0 198.986 294806.884 5629959.884 198.969 294806.966 5629959.559 198.848</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294806.612 5629960.955 198.992 294806.791 5629961.0 198.963 294807.0 5629961.053 198.936 294807.07 5629961.07 198.938 294808.0 5629961.304 199.008 294808.406 5629961.406 199.002 294809.0 5629961.555 198.98 294809.742 5629961.742 198.759 294810.0 5629961.807 198.659 294810.415 5629961.911 198.511</gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString>
                  <gml:posList srsDimension="3">294810.415 5629961.911 198.511 294810.515 5629961.515 198.387 294810.645 5629961.0 198.264 294810.716 5629960.716 198.208 294810.897 5629960.0 198.084 294810.918 5629959.918 198.075 294811.0 5629959.593 198.013 294811.119 5629959.119 197.95 294811.15 5629959.0 197.917 294811.321 5629958.321 197.772 294811.402 5629958.0 197.707 294811.522 5629957.522 197.721 294811.553 5629957.401 197.73</gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:MultiCurve>
          </bldg:lod2TerrainIntersection>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_040cac0c-d479-4d34-9a3d-7a9f0d71a50f">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_a20efeb3-ae06-4479-befe-a04ddc8d15f9">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_2">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_2_0_">
                          <gml:posList srsDimension="3">294806.558 5629956.138 208.583 294805.773 5629959.257 208.583 294805.773 5629959.257 197.392 294806.558 5629956.138 197.392 294806.558 5629956.138 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_a8bbb47d-8800-4c35-a4f5-7af89cadd8b8">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_4d500d37-a74e-41cd-ac60-1ef4dee24dd8">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_3">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_3_0_">
                          <gml:posList srsDimension="3">294805.773 5629959.257 208.583 294806.966 5629959.559 208.583 294806.966 5629959.559 197.392 294805.773 5629959.257 197.392 294805.773 5629959.257 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_6453131c-60cb-4a72-9f33-e8b506b625e8">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_f80f6a6d-16a2-4a4e-8b4f-3a0e599ec33c">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_4">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_4_0_">
                          <gml:posList srsDimension="3">294806.966 5629959.559 208.583 294806.612 5629960.955 208.583 294806.612 5629960.955 197.392 294806.966 5629959.559 197.392 294806.966 5629959.559 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_50870cc1-865b-4c15-b0b9-2bd6e241643a">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_15b04fce-681c-4754-91f2-b1f5492839d3">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_5">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_5_0_">
                          <gml:posList srsDimension="3">294806.612 5629960.955 208.583 294810.415 5629961.911 208.583 294810.415 5629961.911 197.392 294806.612 5629960.955 197.392 294806.612 5629960.955 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_75b6eae2-93fd-4ca4-87d5-f517f8e3225e">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_16b40950-a588-4cce-abc0-d811b9e9cd75">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_6">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_6_0_">
                          <gml:posList srsDimension="3">294810.415 5629961.911 208.583 294811.553 5629957.401 208.583 294811.553 5629957.401 197.392 294810.415 5629961.911 197.392 294810.415 5629961.911 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:RoofSurface gml:id="UUID_28e53e8e-ed49-4b34-9e57-352c969b0996">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_2540f1e4-767f-4a18-aee2-52c9635048fc">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_7">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_7_0_">
                          <gml:posList srsDimension="3">294810.415 5629961.911 208.583 294806.612 5629960.955 208.583 294806.966 5629959.559 208.583 294805.773 5629959.257 208.583 294806.558 5629956.138 208.583 294811.553 5629957.401 208.583 294810.415 5629961.911 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:RoofSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:GroundSurface gml:id="UUID_4ed437c4-8699-4f73-a59c-0e6c996b18ce">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface gml:id="UUID_15a9f6d7-a95c-4272-8f26-789d80f15445">
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="GUID_1543405942858_7180440_2_0">
                      <gml:exterior>
                        <gml:LinearRing gml:id="GUID_1543405942858_7180440_2_0_0_">
                          <gml:posList srsDimension="3">294811.553 5629957.401 197.392 294806.558 5629956.138 197.392 294805.773 5629959.257 197.392 294806.966 5629959.559 197.392 294806.612 5629960.955 197.392 294810.415 5629961.911 197.392 294811.553 5629957.401 197.392</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:GroundSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:WallSurface gml:id="UUID_a84a0f0b-d4df-4001-a82b-1f93d444d862">
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_f05036fc-7c20-41b1-ac88-16d8dc994df2">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294811.553 5629957.401 208.583 294811.514 5629957.391 208.583 294811.514 5629957.391 197.392 294806.558 5629956.138 197.392 294811.553 5629957.401 197.392 294811.553 5629957.401 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_87ea3ad8-ead8-4803-8278-a70882d4f78c">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294807.552 5629956.389 208.583 294806.558 5629956.138 208.583 294806.558 5629956.138 207.339 294807.552 5629956.389 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:WallSurface>
          </bldg:boundedBy>
          <bldg:boundedBy>
            <bldg:ClosureSurface>
              <core:creationDate>2018-12-21</core:creationDate>
              <bldg:lod2MultiSurface>
                <gml:MultiSurface>
                  <gml:surfaceMember>
                    <gml:Polygon gml:id="UUID_852b6b99-8c92-48bf-953e-5061dec20caa">
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList srsDimension="3">294811.514 5629957.391 208.583 294807.552 5629956.389 208.583 294806.558 5629956.138 207.339 294806.558 5629956.138 197.392 294811.514 5629957.391 197.392 294811.514 5629957.391 208.583</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </bldg:lod2MultiSurface>
            </bldg:ClosureSurface>
          </bldg:boundedBy>
        </bldg:BuildingPart>
      </bldg:consistsOfBuildingPart>
      <bldg:address>
        <core:Address gml:id="UUID_3a729da9-b716-4960-92fb-2adb389015ab">
          <core:xalAddress>
            <xal:AddressDetails>
              <xal:Country>
                <xal:CountryName>Germany</xal:CountryName>
                <xal:Locality Type="Town">
                  <xal:LocalityName>Aachen</xal:LocalityName>
                  <xal:Thoroughfare Type="Street">
                    <xal:ThoroughfareNumber>12</xal:ThoroughfareNumber>
                    <xal:ThoroughfareName>Elsa-Brändström-Straße</xal:ThoroughfareName>
                  </xal:Thoroughfare>
                  <xal:PostalCode>
                    <xal:PostalCodeNumber>52070</xal:PostalCodeNumber>
                  </xal:PostalCode>
                </xal:Locality>
              </xal:Country>
            </xal:AddressDetails>
          </core:xalAddress>
        </core:Address>
      </bldg:address>
    </bldg:Building>
  </core:cityObjectMember>
  <core:cityObjectMember>
    <bldg:Building gml:id="DENW39AL1000Mje8">
      <core:creationDate>2018-12-21</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
        <core:externalObject>
          <core:name>DENW39AL1000Mje8</core:name>
        </core:externalObject>
      </core:externalReference>
      <gen:stringAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Gemeindeschluessel">
        <gen:value>05334002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <bldg:function>31001_1010</bldg:function>
      <bldg:yearOfConstruction>1975</bldg:yearOfConstruction>
      <bldg:roofType>3100</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">20.687</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_5"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_2"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_1"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_6"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_7"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_4"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_0"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_3"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_8"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180183_2_9"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:lod2TerrainIntersection>
        <gml:MultiCurve>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294069.823 5629771.969 189.006 294069.852 5629772.0 189.008 294070.0 5629772.159 189.036 294070.786 5629773.0 189.23 294071.0 5629773.23 189.274 294071.719 5629774.0 189.349 294072.0 5629774.301 189.39 294072.653 5629775.0 189.525 294073.0 5629775.372 189.579 294073.586 5629776.0 189.639 294074.0 5629776.443 189.713 294074.52 5629777.0 189.832 294074.564 5629777.047 189.839</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294065.165 5629776.332 188.73 294065.251 5629776.251 188.738 294065.519 5629776.0 188.751 294065.768 5629775.768 188.763 294066.0 5629775.55 188.788 294066.284 5629775.284 188.847 294066.587 5629775.0 188.845 294066.8 5629774.8 188.852 294067.0 5629774.613 188.862 294067.317 5629774.317 188.907 294067.655 5629774.0 188.87 294067.833 5629773.833 188.87 294068.0 5629773.677 188.877 294068.349 5629773.349 188.929 294068.722 5629773.0 188.928 294068.866 5629772.866 188.935 294069.0 5629772.74 188.947 294069.382 5629772.382 189.009 294069.79 5629772.0 189.003 294069.823 5629771.969 189.006</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294065.165 5629776.332 188.73 294065.788 5629777.0 188.747 294066.0 5629777.228 188.774 294066.72 5629778.0 189.007 294067.0 5629778.3 189.119 294067.206 5629778.521 189.17</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294067.206 5629778.521 189.17 294067.653 5629779.0 189.282 294068.0 5629779.373 189.316 294068.585 5629780.0 189.439 294069.0 5629780.445 189.579 294069.517 5629781.0 189.676 294069.885 5629781.395 189.714</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294069.885 5629781.395 189.714 294070.0 5629781.518 189.726 294070.449 5629782.0 189.83 294071.0 5629782.591 190.005 294071.381 5629783.0 190.073 294072.0 5629783.664 190.156 294072.314 5629784.0 190.202 294073.0 5629784.736 190.383 294073.246 5629785.0 190.469 294074.0 5629785.809 190.711 294074.178 5629786.0 190.745 294074.605 5629786.458 190.763</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294074.605 5629786.458 190.763 294075.0 5629786.094 190.771 294075.049 5629786.049 190.77 294075.102 5629786.0 190.771 294075.569 5629785.569 190.746 294076.0 5629785.171 190.763 294076.089 5629785.089 190.757 294076.186 5629785.0 190.764 294076.609 5629784.609 190.741 294077.0 5629784.249 190.757 294077.13 5629784.13 190.747 294077.27 5629784.0 190.753 294077.65 5629783.65 190.714 294078.0 5629783.327 190.726 294078.17 5629783.17 190.708 294078.354 5629783.0 190.71 294078.69 5629782.69 190.685 294079.0 5629782.404 190.698 294079.21 5629782.21 190.688 294079.304 5629782.124 190.692</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294074.564 5629777.047 189.839 294075.0 5629777.514 189.906 294075.454 5629778.0 189.957 294076.0 5629778.585 190.078 294076.387 5629779.0 190.159 294077.0 5629779.656 190.243 294077.321 5629780.0 190.312 294078.0 5629780.727 190.495 294078.255 5629781.0 190.558 294079.0 5629781.798 190.68 294079.188 5629782.0 190.694 294079.304 5629782.124 190.692</gml:posList>
            </gml:LineString>
          </gml:curveMember>
        </gml:MultiCurve>
      </bldg:lod2TerrainIntersection>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_937cc0f5-f921-460c-8eff-7b88c5b19eb0">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_71a21b36-be8f-4134-822f-831ec61c6ff0">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_1_0_">
                      <gml:posList srsDimension="3">294074.564 5629777.047 209.417 294069.823 5629771.969 204.413 294069.823 5629771.969 188.73 294074.564 5629777.047 188.73 294074.564 5629777.047 209.417</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_fe504ba1-de13-49f8-9b16-128a98429a3c">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_fdd3786b-6756-4810-8f54-09f5b2419f2d">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_2_0_">
                      <gml:posList srsDimension="3">294069.823 5629771.969 204.413 294065.165 5629776.332 204.413 294065.165 5629776.332 188.73 294069.823 5629771.969 188.73 294069.823 5629771.969 204.413</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_024b8b0e-7cc3-4b54-98f7-6ad248256d75">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_aed1422a-3436-4283-b9a9-44fa84096243">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_3_0_">
                      <gml:posList srsDimension="3">294065.165 5629776.332 204.413 294067.206 5629778.521 206.577 294067.206 5629778.521 188.73 294065.165 5629776.332 188.73 294065.165 5629776.332 204.413</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_bb505909-91f9-4a2f-b9ab-0983e79984ae">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_e5962efc-b33a-45f8-b89e-c2a5551e8755">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_4_0_">
                      <gml:posList srsDimension="3">294067.206 5629778.521 206.577 294069.885 5629781.395 209.417 294069.885 5629781.395 188.73 294067.206 5629778.521 188.73 294067.206 5629778.521 206.577</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9cb9edd7-6f27-4680-91c2-9971f0b57cff">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_fe672e25-9aa6-4609-ae1d-9e96d339c166">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_5_0_">
                      <gml:posList srsDimension="3">294069.885 5629781.395 209.417 294074.605 5629786.458 204.413 294074.605 5629786.458 188.73 294069.885 5629781.395 188.73 294069.885 5629781.395 209.417</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_48af381e-c70d-483c-8cba-ae9191cccd85">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3a18e318-0a71-468f-bb7d-0b99ff48d791">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_6_0_">
                      <gml:posList srsDimension="3">294074.605 5629786.458 204.413 294079.304 5629782.124 204.413 294079.304 5629782.124 188.73 294074.605 5629786.458 188.73 294074.605 5629786.458 204.413</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_cb844f1f-adfa-4f54-b2fe-4003b2df0b01">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_b61ed757-1bb7-4f04-aa25-1514189ec6fa">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_7_0_">
                      <gml:posList srsDimension="3">294079.304 5629782.124 204.413 294074.564 5629777.047 209.417 294074.564 5629777.047 188.73 294079.304 5629782.124 188.73 294079.304 5629782.124 204.413</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_195e1949-92f3-4425-b35c-c3d77f60e3f4">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d8b131e2-6d18-4a7e-827f-9e4c4059eb90">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_8_0_">
                      <gml:posList srsDimension="3">294069.885 5629781.395 209.417 294067.206 5629778.521 206.577 294065.165 5629776.332 204.413 294069.823 5629771.969 204.413 294074.564 5629777.047 209.417 294069.885 5629781.395 209.417</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_ea1ba632-f219-4a51-843d-98f45a8a158a">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2450d104-6584-457c-b94f-09dc7eb2f957">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_9_0_">
                      <gml:posList srsDimension="3">294074.564 5629777.047 209.417 294079.304 5629782.124 204.413 294074.605 5629786.458 204.413 294069.885 5629781.395 209.417 294074.564 5629777.047 209.417</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_6ebb1ede-d8f9-4562-b6bc-5145720c63bd">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_6e63ac35-975e-4a26-ae73-ca2f2733b750">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180183_2_0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180183_2_0_0_">
                      <gml:posList srsDimension="3">294074.564 5629777.047 188.73 294069.823 5629771.969 188.73 294065.165 5629776.332 188.73 294067.206 5629778.521 188.73 294069.885 5629781.395 188.73 294074.605 5629786.458 188.73 294079.304 5629782.124 188.73 294074.564 5629777.047 188.73</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:address>
        <core:Address gml:id="UUID_74bb7439-1a32-439c-9665-51beed41f126">
          <core:xalAddress>
            <xal:AddressDetails>
              <xal:Country>
                <xal:CountryName>Germany</xal:CountryName>
                <xal:Locality Type="Town">
                  <xal:LocalityName>Aachen</xal:LocalityName>
                  <xal:Thoroughfare Type="Street">
                    <xal:ThoroughfareNumber>17</xal:ThoroughfareNumber>
                    <xal:ThoroughfareName>Lousbergstraße</xal:ThoroughfareName>
                  </xal:Thoroughfare>
                  <xal:PostalCode>
                    <xal:PostalCodeNumber>52072</xal:PostalCodeNumber>
                  </xal:PostalCode>
                </xal:Locality>
              </xal:Country>
            </xal:AddressDetails>
          </core:xalAddress>
        </core:Address>
      </bldg:address>
    </bldg:Building>
  </core:cityObjectMember>
  <core:cityObjectMember>
    <bldg:Building gml:id="DENW39AL10005X5J">
      <core:creationDate>2018-12-21</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
        <core:externalObject>
          <core:name>DENW39AL10005X5J</core:name>
        </core:externalObject>
      </core:externalReference>
      <gen:stringAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Gemeindeschluessel">
        <gen:value>05334002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <bldg:function>31001_1010</bldg:function>
      <bldg:yearOfConstruction>1975</bldg:yearOfConstruction>
      <bldg:roofType>3100</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">20.28</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_10"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_2"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_6"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_9"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_8"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_0"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_5"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_4"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_3"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_1"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7180173_2_7"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:lod2TerrainIntersection>
        <gml:MultiCurve>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294418.326 5629573.385 183.722 294418.361 5629573.361 183.719 294418.892 5629573.0 183.681 294418.956 5629572.956 183.675 294419.0 5629572.926 183.671 294419.551 5629572.551 183.61 294420.0 5629572.246 183.56 294420.146 5629572.146 183.545 294420.362 5629572.0 183.523 294420.742 5629571.742 183.487 294421.0 5629571.566 183.449 294421.337 5629571.337 183.387 294421.831 5629571.0 183.254 294421.932 5629570.932 183.231 294422.0 5629570.885 183.213 294422.527 5629570.527 183.08 294423.0 5629570.205 182.889 294423.122 5629570.122 182.856 294423.185 5629570.079 182.849</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294413.095 5629576.944 184.366 294413.6 5629576.6 184.342 294414.0 5629576.328 184.263 294414.195 5629576.195 184.234 294414.482 5629576.0 184.22 294414.79 5629575.79 184.178 294415.0 5629575.648 184.135 294415.386 5629575.386 184.038 294415.952 5629575.0 183.904 294415.981 5629574.981 183.898 294416.0 5629574.968 183.897 294416.576 5629574.576 183.85 294417.0 5629574.287 183.816 294417.171 5629574.171 183.805 294417.422 5629574.0 183.79 294417.766 5629573.766 183.771 294418.0 5629573.607 183.751 294418.326 5629573.385 183.722</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294413.095 5629576.944 184.366 294413.133 5629577.0 184.367 294413.414 5629577.414 184.399 294413.812 5629578.0 184.427 294414.0 5629578.277 184.451 294414.491 5629579.0 184.509 294415.0 5629579.75 184.607 294415.17 5629580.0 184.628 294415.53 5629580.53 184.694 294415.849 5629581.0 184.732 294416.0 5629581.222 184.752 294416.528 5629582.0 184.797 294417.0 5629582.695 184.872 294417.097 5629582.838 184.886</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294417.097 5629582.838 184.886 294417.207 5629583.0 184.902 294417.65 5629583.65 184.961 294417.888 5629584.0 184.991 294418.0 5629584.164 185.008 294418.569 5629585.0 185.073 294419.0 5629585.633 185.134 294419.25 5629586.0 185.178 294419.784 5629586.784 185.27 294419.931 5629587.0 185.293 294420.0 5629587.101 185.302 294420.612 5629588.0 185.332 294421.0 5629588.57 185.334 294421.293 5629589.0 185.312 294421.917 5629589.917 185.284 294421.974 5629590.0 185.283 294422.0 5629590.039 185.291 294422.655 5629591.0 185.632 294423.0 5629591.507 185.69 294423.185 5629591.779 185.718</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294423.185 5629591.779 185.718 294423.539 5629591.539 185.74 294424.0 5629591.226 185.8 294424.134 5629591.134 185.809 294424.333 5629591.0 185.817 294424.73 5629590.73 185.81 294425.0 5629590.547 185.85 294425.326 5629590.326 185.87 294425.806 5629590.0 185.89 294425.921 5629589.921 185.889 294426.0 5629589.868 185.889 294426.517 5629589.517 185.851 294427.0 5629589.189 185.856 294427.113 5629589.113 185.85 294427.279 5629589.0 185.842 294427.708 5629588.708 185.794 294428.0 5629588.511 185.786 294428.304 5629588.304 185.762 294428.416 5629588.229 185.763</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294428.416 5629588.229 185.763 294428.752 5629588.0 185.765 294428.9 5629587.9 185.745 294429.0 5629587.832 185.743 294429.495 5629587.495 185.674 294430.0 5629587.153 185.664 294430.091 5629587.091 185.655 294430.225 5629587.0 185.655 294430.687 5629586.687 185.532 294431.0 5629586.474 185.544 294431.282 5629586.282 185.492 294431.698 5629586.0 185.514 294431.878 5629585.878 185.478 294432.0 5629585.795 185.399 294432.474 5629585.474 185.161 294433.0 5629585.117 184.834 294433.069 5629585.069 184.797 294433.172 5629585.0 184.773 294433.646 5629584.678 184.483</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294423.555 5629569.823 182.801 294423.675 5629570.0 182.833 294424.0 5629570.478 182.897 294424.355 5629571.0 182.959 294425.0 5629571.95 182.969 294425.034 5629572.0 182.968 294425.106 5629572.106 182.963 294425.713 5629573.0 182.952 294426.0 5629573.422 182.989 294426.392 5629574.0 183.024 294427.0 5629574.894 182.962 294427.072 5629575.0 182.961 294427.224 5629575.224 182.983 294427.751 5629576.0 183.035 294428.0 5629576.367 183.001 294428.43 5629577.0 182.911 294429.0 5629577.839 182.919 294429.11 5629578.0 182.923 294429.342 5629578.342 182.927 294429.789 5629579.0 182.939 294430.0 5629579.311 182.96 294430.468 5629580.0 183.031 294431.0 5629580.783 183.065 294431.148 5629581.0 183.074 294431.46 5629581.46 183.08 294431.827 5629582.0 183.104 294432.0 5629582.255 183.156 294432.506 5629583.0 183.284 294433.0 5629583.727 183.459 294433.185 5629584.0 183.508 294433.578 5629584.578 184.352 294433.646 5629584.678 184.483</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294423.185 5629570.079 182.849 294423.299 5629570.0 182.837 294423.555 5629569.823 182.801</gml:posList>
            </gml:LineString>
          </gml:curveMember>
        </gml:MultiCurve>
      </bldg:lod2TerrainIntersection>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d6a1a6ed-13e2-462b-b36f-ba63999a7cb6">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3b8c96e2-6cc5-404a-8cdb-3a9c8fbca8f6">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_1_0_">
                      <gml:posList srsDimension="3">294423.185 5629570.079 199.413 294418.326 5629573.385 203.081 294418.326 5629573.385 182.801 294423.185 5629570.079 182.801 294423.185 5629570.079 199.413</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_268ba030-3b4c-4f97-ba98-3825e9adf871">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ea400d3a-5b6e-4610-b272-d29c7a5c3a0a">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_2_0_">
                      <gml:posList srsDimension="3">294418.326 5629573.385 203.081 294413.095 5629576.944 199.132 294413.095 5629576.944 182.801 294418.326 5629573.385 182.801 294418.326 5629573.385 203.081</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2fe48019-ee65-449d-af2a-9a086c0d680a">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_67516cea-ce18-49c2-9ebb-612812ca727d">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_3_0_">
                      <gml:posList srsDimension="3">294413.095 5629576.944 199.132 294417.097 5629582.838 199.129 294417.097 5629582.838 182.801 294413.095 5629576.944 182.801 294413.095 5629576.944 199.132</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_50cba91a-8a41-4ce8-b678-ad88029c7c7c">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_14c1d077-f911-4342-9d56-f2f6e6928520">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_4_0_">
                      <gml:posList srsDimension="3">294417.097 5629582.838 199.129 294423.185 5629591.779 199.132 294423.185 5629591.779 182.801 294417.097 5629582.838 182.801 294417.097 5629582.838 199.129</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d43c40d4-5138-4f6d-97a2-d8613b80304a">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ae92b1c5-37b8-49c5-ba86-55298cb8480d">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_5_0_">
                      <gml:posList srsDimension="3">294423.185 5629591.779 199.132 294428.416 5629588.229 203.081 294428.416 5629588.229 182.801 294423.185 5629591.779 182.801 294423.185 5629591.779 199.132</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ce025e0f-7853-4486-bbc4-0a5782d1ec43">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_17b95abb-cd8c-4731-b54c-78771d9b091b">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_6_0_">
                      <gml:posList srsDimension="3">294428.416 5629588.229 203.081 294433.646 5629584.678 199.132 294433.646 5629584.678 182.801 294428.416 5629588.229 182.801 294428.416 5629588.229 203.081</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_843b00a8-41ea-4970-a566-5efe25b9dbb3">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_03c80d49-5578-42b1-aa4d-360bca28806f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_7_0_">
                      <gml:posList srsDimension="3">294433.646 5629584.678 199.132 294423.555 5629569.823 199.132 294423.555 5629569.823 182.801 294433.646 5629584.678 182.801 294433.646 5629584.678 199.132</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_41bd57eb-ea61-45ef-8ef9-0646b593fc64">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_d21851f2-4952-4ea1-a728-3f2c39c8b089">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_8_0_">
                      <gml:posList srsDimension="3">294423.555 5629569.823 199.132 294423.185 5629570.079 199.413 294423.185 5629570.079 182.801 294423.555 5629569.823 182.801 294423.555 5629569.823 199.132</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_c636ce4e-324a-4af7-a99b-b576c2423d0e">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_8c6cae8a-a751-43ea-879f-a6a06b05cb61">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_9_0_">
                      <gml:posList srsDimension="3">294428.416 5629588.229 203.081 294423.185 5629591.779 199.132 294417.097 5629582.838 199.129 294413.095 5629576.944 199.132 294418.326 5629573.385 203.081 294428.416 5629588.229 203.081</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_971e95c0-50e2-4b67-a2bd-431f472e6d35">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_81641a25-adb4-4057-91d9-40febe2f347f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_10">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_10_0_">
                      <gml:posList srsDimension="3">294418.326 5629573.385 203.081 294423.185 5629570.079 199.413 294423.555 5629569.823 199.132 294433.646 5629584.678 199.132 294428.416 5629588.229 203.081 294418.326 5629573.385 203.081</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_51526ee9-7a72-47ec-b3e8-e0f89d41df4d">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_187e44cd-9dfa-45c9-9103-fd7920512565">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7180173_2_0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7180173_2_0_0_">
                      <gml:posList srsDimension="3">294423.185 5629570.079 182.801 294418.326 5629573.385 182.801 294413.095 5629576.944 182.801 294417.097 5629582.838 182.801 294423.185 5629591.779 182.801 294428.416 5629588.229 182.801 294433.646 5629584.678 182.801 294423.555 5629569.823 182.801 294423.185 5629570.079 182.801</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:address>
        <core:Address gml:id="UUID_40d6e6f2-cc7b-4a48-87f8-ed28ceaaba04">
          <core:xalAddress>
            <xal:AddressDetails>
              <xal:Country>
                <xal:CountryName>Germany</xal:CountryName>
                <xal:Locality Type="Town">
                  <xal:LocalityName>Aachen</xal:LocalityName>
                  <xal:Thoroughfare Type="Street">
                    <xal:ThoroughfareNumber>21</xal:ThoroughfareNumber>
                    <xal:ThoroughfareName>Veltmanplatz</xal:ThoroughfareName>
                  </xal:Thoroughfare>
                  <xal:PostalCode>
                    <xal:PostalCodeNumber>52062</xal:PostalCodeNumber>
                  </xal:PostalCode>
                </xal:Locality>
              </xal:Country>
            </xal:AddressDetails>
          </core:xalAddress>
        </core:Address>
      </bldg:address>
    </bldg:Building>
  </core:cityObjectMember>
  <core:cityObjectMember>
    <bldg:Building gml:id="DENW39AL10005VOB">
      <core:creationDate>2018-12-21</core:creationDate>
      <core:externalReference>
        <core:informationSystem>http://repository.gdi-de.org/schemas/adv/citygml/fdv/art.htm#_9100</core:informationSystem>
        <core:externalObject>
          <core:name>DENW39AL10005VOB</core:name>
        </core:externalObject>
      </core:externalReference>
      <gen:stringAttribute name="DatenquelleDachhoehe">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="Gemeindeschluessel">
        <gen:value>05334002</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleBodenhoehe">
        <gen:value>1100</gen:value>
      </gen:stringAttribute>
      <gen:stringAttribute name="DatenquelleLage">
        <gen:value>1000</gen:value>
      </gen:stringAttribute>
      <bldg:function>31001_1010</bldg:function>
      <bldg:yearOfConstruction>2005</bldg:yearOfConstruction>
      <bldg:roofType>5000</bldg:roofType>
      <bldg:measuredHeight uom="urn:adv:uom:m">5.085</bldg:measuredHeight>
      <bldg:lod2Solid>
        <gml:Solid>
          <gml:exterior>
            <gml:CompositeSurface>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_13"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_5"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_1"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_12"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_0"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_17"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_11"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_16"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_18"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_9"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_10"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_8"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_4"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_15"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_2"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_14"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_7"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_3"/>
              <gml:surfaceMember xlink:href="#GUID_1543405942858_7179895_2_6"/>
            </gml:CompositeSurface>
          </gml:exterior>
        </gml:Solid>
      </bldg:lod2Solid>
      <bldg:lod2TerrainIntersection>
        <gml:MultiCurve>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294165.225 5629383.647 175.827 294165.527 5629383.695 175.817</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294165.527 5629383.695 175.817 294165.724 5629383.724 175.81 294166.0 5629383.764 175.798 294166.332 5629383.812 175.794</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294166.332 5629383.812 175.794 294166.398 5629383.398 175.772 294166.461 5629383.0 175.755 294166.535 5629382.535 175.745 294166.62 5629382.0 175.728 294166.672 5629381.672 175.713 294166.779 5629381.0 175.684 294166.809 5629380.809 175.68 294166.907 5629380.195 175.666</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294165.436 5629379.955 175.701 294165.712 5629380.0 175.692 294166.0 5629380.047 175.681 294166.056 5629380.056 175.68 294166.907 5629380.195 175.666</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294165.436 5629379.955 175.701 294165.519 5629379.519 175.68 294165.617 5629379.0 175.655 294165.678 5629378.678 175.646 294165.806 5629378.0 175.628 294165.837 5629377.837 175.618 294165.996 5629377.0 175.57 294165.996 5629376.996 175.57 294166.0 5629376.976 175.57 294166.03 5629376.818 175.566</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294154.724 5629374.545 175.812 294155.0 5629374.6 175.802 294155.752 5629374.752 175.79 294156.0 5629374.802 175.786 294156.987 5629375.0 175.76 294157.0 5629375.003 175.76 294157.003 5629375.003 175.76 294158.0 5629375.204 175.742 294158.255 5629375.255 175.735 294159.0 5629375.405 175.72 294159.506 5629375.506 175.715 294160.0 5629375.606 175.706 294160.758 5629375.758 175.693 294161.0 5629375.807 175.692 294161.961 5629376.0 175.671 294162.0 5629376.008 175.67 294162.01 5629376.01 175.67 294163.0 5629376.209 175.654 294163.261 5629376.261 175.645 294164.0 5629376.41 175.618 294164.513 5629376.513 175.61 294165.0 5629376.611 175.602 294165.765 5629376.765 175.575 294166.0 5629376.812 175.566 294166.03 5629376.818 175.566</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294154.423 5629376.038 175.884 294154.431 5629376.0 175.883 294154.526 5629375.526 175.851 294154.632 5629375.0 175.821 294154.694 5629374.694 175.816 294154.724 5629374.545 175.812</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294153.16 5629375.789 175.904 294153.943 5629375.943 175.898 294154.0 5629375.955 175.897 294154.23 5629376.0 175.891 294154.423 5629376.038 175.884</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294152.793 5629378.164 175.969 294152.819 5629378.0 175.964 294152.843 5629377.843 175.962 294152.973 5629377.0 175.941 294152.977 5629376.977 175.94 294153.0 5629376.825 175.937 294153.11 5629376.11 175.92 294153.127 5629376.0 175.917 294153.16 5629375.789 175.904</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294152.765 5629378.347 175.975 294152.793 5629378.164 175.969</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294152.205 5629382.034 176.096 294152.21 5629382.0 176.094 294152.314 5629381.314 176.049 294152.362 5629381.0 176.04 294152.446 5629380.446 176.012 294152.514 5629380.0 176.0 294152.578 5629379.578 176.002 294152.666 5629379.0 175.99 294152.71 5629378.71 175.987 294152.765 5629378.347 175.975</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294152.205 5629382.034 176.096 294153.0 5629382.086 176.074 294153.092 5629382.092 176.073 294154.0 5629382.152 176.083 294154.163 5629382.163 176.08 294155.0 5629382.218 176.057 294155.234 5629382.234 176.055 294156.0 5629382.284 176.041 294156.304 5629382.304 176.033 294156.622 5629382.325 176.027</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294156.622 5629382.325 176.027 294157.0 5629382.373 176.021 294157.427 5629382.427 176.01 294158.0 5629382.499 175.995 294158.571 5629382.571 175.98 294159.0 5629382.626 175.969 294159.716 5629382.716 175.95 294160.0 5629382.752 175.945 294160.861 5629382.861 175.93 294161.0 5629382.878 175.926 294161.963 5629383.0 175.891 294162.0 5629383.005 175.89 294162.005 5629383.005 175.89 294163.0 5629383.131 175.875 294163.15 5629383.15 175.87 294164.0 5629383.257 175.833 294164.295 5629383.295 175.829 294164.788 5629383.357 175.826</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294164.788 5629383.357 175.826 294165.0 5629383.384 175.825 294165.255 5629383.416 175.816</gml:posList>
            </gml:LineString>
          </gml:curveMember>
          <gml:curveMember>
            <gml:LineString>
              <gml:posList srsDimension="3">294165.225 5629383.647 175.827 294165.255 5629383.416 175.816</gml:posList>
            </gml:LineString>
          </gml:curveMember>
        </gml:MultiCurve>
      </bldg:lod2TerrainIntersection>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_fdbbbee8-c63f-4b90-b721-570ca528e759">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5d7546a2-ee37-4c49-9a34-1554d884d579">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_1">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_1_0_">
                      <gml:posList srsDimension="3">294165.225 5629383.647 178.106 294165.527 5629383.695 178.096 294165.527 5629383.695 175.566 294165.225 5629383.647 175.566 294165.225 5629383.647 178.106</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_51b0077f-7260-4870-9e7e-5c9134fce457">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_147b2454-b244-4122-a00a-3fd4bb37a209">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_2">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_2_0_">
                      <gml:posList srsDimension="3">294165.527 5629383.695 178.096 294166.332 5629383.812 178.076 294166.332 5629383.812 175.566 294165.527 5629383.695 175.566 294165.527 5629383.695 178.096</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_a4fc865c-3c52-4aaf-a4aa-47ecbf349a94">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_4ed2550c-0e96-4908-a424-06a292cf8c4a">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_3">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_3_0_">
                      <gml:posList srsDimension="3">294166.332 5629383.812 178.076 294166.907 5629380.195 175.841 294166.907 5629380.195 175.566 294166.332 5629383.812 175.566 294166.332 5629383.812 178.076</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_6877467b-7e7f-48bf-b48f-a750fc6fca05">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_2e920ec1-6a07-4c1e-a3de-d6b340d7c3b3">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_4">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_4_0_">
                      <gml:posList srsDimension="3">294166.907 5629380.195 175.841 294165.436 5629379.955 178.249 294165.436 5629379.955 175.566 294166.907 5629380.195 175.566 294166.907 5629380.195 175.841</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_46e2dc35-d320-4381-8a5f-be005de49bb3">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_20a99f59-b320-46ce-a4e9-4cf9b216596b">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_5">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_5_0_">
                      <gml:posList srsDimension="3">294165.436 5629379.955 178.249 294166.03 5629376.818 178.249 294166.03 5629376.818 175.566 294165.436 5629379.955 175.566 294165.436 5629379.955 178.249</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_052ec0e4-957a-46ef-b0d2-b22748bfe23f">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_186d01f9-02a1-483f-ae78-aa223a16a883">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_6">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_6_0_">
                      <gml:posList srsDimension="3">294166.03 5629376.818 178.249 294154.724 5629374.545 178.249 294154.724 5629374.545 175.566 294166.03 5629376.818 175.566 294166.03 5629376.818 178.249</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_ee87c179-33ec-4069-94f1-0d526efef56b">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1d458a99-a79f-494b-80ac-789f68a7e8cc">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_7">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_7_0_">
                      <gml:posList srsDimension="3">294154.724 5629374.545 178.249 294154.423 5629376.038 179.18 294154.423 5629376.038 175.566 294154.724 5629374.545 175.566 294154.724 5629374.545 178.249</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_66cdb8c7-7b30-469d-8810-cb67454231d0">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a6f4212f-d346-48e9-a0c4-786eb0812733">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_8">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_8_0_">
                      <gml:posList srsDimension="3">294154.423 5629376.038 179.18 294153.16 5629375.789 179.146 294153.16 5629375.789 175.566 294154.423 5629376.038 175.566 294154.423 5629376.038 179.18</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_5d0a92aa-114a-4b26-aa57-64f486d6e810">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5bb13481-39b0-40b5-829c-bd72866d8fd9">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_9">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_9_0_">
                      <gml:posList srsDimension="3">294153.16 5629375.789 179.146 294152.793 5629378.164 180.651 294152.793 5629378.164 175.566 294153.16 5629375.789 175.566 294153.16 5629375.789 179.146</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_96daac80-4955-467f-b35d-9f05b8498957">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_1091339f-a7b4-4ad0-aca6-0c8efc758157">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_10">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_10_0_">
                      <gml:posList srsDimension="3">294152.793 5629378.164 180.651 294152.765 5629378.347 180.538 294152.765 5629378.347 175.566 294152.793 5629378.164 175.566 294152.793 5629378.164 180.651</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_2901f363-309e-44aa-81ff-e90132b08294">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a596711b-0083-4076-a7f6-2a31f367747c">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_11">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_11_0_">
                      <gml:posList srsDimension="3">294152.765 5629378.347 180.538 294152.205 5629382.034 178.249 294152.205 5629382.034 175.566 294152.765 5629378.347 175.566 294152.765 5629378.347 180.538</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_bfe2550e-5d20-408d-a661-67ea661e7606">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_97a0ae1a-f19e-48b1-88c4-e0378ed50b58">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_12">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_12_0_">
                      <gml:posList srsDimension="3">294152.205 5629382.034 178.249 294156.622 5629382.325 178.357 294156.622 5629382.325 175.566 294152.205 5629382.034 175.566 294152.205 5629382.034 178.249</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_10e29fe2-9862-43d9-8099-d55e14653463">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_a227bcfd-51ec-4bbc-836f-a9888bc59f4f">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_13">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_13_0_">
                      <gml:posList srsDimension="3">294156.622 5629382.325 178.357 294164.788 5629383.357 178.255 294164.788 5629383.357 175.566 294156.622 5629382.325 175.566 294156.622 5629382.325 178.357</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_9a3d6386-ffb2-437a-b2ff-5d61269aec7b">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_3bb924c7-ef88-4eec-a935-cf04efbf4b7c">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_14">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_14_0_">
                      <gml:posList srsDimension="3">294164.788 5629383.357 178.255 294165.255 5629383.416 177.495 294165.255 5629383.416 175.566 294164.788 5629383.357 175.566 294164.788 5629383.357 178.255</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:WallSurface gml:id="UUID_d8cb0d8d-9f9d-4f20-ad8a-66ba9b308b00">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_25b67954-c884-46ad-b8c1-c84b1a0524bd">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_15">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_15_0_">
                      <gml:posList srsDimension="3">294165.255 5629383.416 177.495 294165.225 5629383.647 178.106 294165.225 5629383.647 175.566 294165.255 5629383.416 175.566 294165.255 5629383.416 177.495</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:WallSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_ffdfd222-d896-47c8-a0de-d6e022f880a6">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ec81eca7-b9d0-4ac0-88fd-a685e19f8f95">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_16">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_16_0_">
                      <gml:posList srsDimension="3">294163.96 5629379.871 180.618 294152.793 5629378.164 180.651 294153.16 5629375.789 179.146 294154.423 5629376.038 179.18 294154.724 5629374.545 178.249 294166.03 5629376.818 178.249 294163.96 5629379.871 180.618</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_935fe73e-c65e-402e-8e3f-0fdf55582832">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_5b056096-19fd-4b9d-a59e-7ec08e9ed1c9">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_17">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_17_0_">
                      <gml:posList srsDimension="3">294163.96 5629379.871 180.618 294166.03 5629376.818 178.249 294165.436 5629379.955 178.249 294166.907 5629380.195 175.841 294166.332 5629383.812 178.076 294165.527 5629383.695 178.096 294165.225 5629383.647 178.106 294165.255 5629383.416 177.495 294164.788 5629383.357 178.255 294163.96 5629379.871 180.618</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:RoofSurface gml:id="UUID_bed9e0fd-5787-4a4b-9774-1e2da39d2fa0">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_0f323cc4-c088-4af8-9361-8ac2e56e31b0">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_18">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_18_0_">
                      <gml:posList srsDimension="3">294163.96 5629379.871 180.618 294164.788 5629383.357 178.255 294156.622 5629382.325 178.357 294152.205 5629382.034 178.249 294152.765 5629378.347 180.538 294152.793 5629378.164 180.651 294163.96 5629379.871 180.618</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:RoofSurface>
      </bldg:boundedBy>
      <bldg:boundedBy>
        <bldg:GroundSurface gml:id="UUID_47341c5d-5307-4aad-999e-5cc7bd139c2a">
          <core:creationDate>2018-12-21</core:creationDate>
          <bldg:lod2MultiSurface>
            <gml:MultiSurface gml:id="UUID_ae48042e-4cd7-4316-b209-9b03597c0313">
              <gml:surfaceMember>
                <gml:Polygon gml:id="GUID_1543405942858_7179895_2_0">
                  <gml:exterior>
                    <gml:LinearRing gml:id="GUID_1543405942858_7179895_2_0_0_">
                      <gml:posList srsDimension="3">294165.225 5629383.647 175.566 294165.527 5629383.695 175.566 294166.332 5629383.812 175.566 294166.907 5629380.195 175.566 294165.436 5629379.955 175.566 294166.03 5629376.818 175.566 294154.724 5629374.545 175.566 294154.423 5629376.038 175.566 294153.16 5629375.789 175.566 294152.793 5629378.164 175.566 294152.765 5629378.347 175.566 294152.205 5629382.034 175.566 294156.622 5629382.325 175.566 294164.788 5629383.357 175.566 294165.255 5629383.416 175.566 294165.225 5629383.647 175.566</gml:posList>
                    </gml:LinearRing>
                  </gml:exterior>
                </gml:Polygon>
              </gml:surfaceMember>
            </gml:MultiSurface>
          </bldg:lod2MultiSurface>
        </bldg:GroundSurface>
      </bldg:boundedBy>
      <bldg:address>
        <core:Address gml:id="UUID_67526593-3686-4250-b92a-3a587be8b337">
          <core:xalAddress>
            <xal:AddressDetails>
              <xal:Country>
                <xal:CountryName>Germany</xal:CountryName>
                <xal:Locality Type="Town">
                  <xal:LocalityName>Aachen</xal:LocalityName>
                  <xal:Thoroughfare Type="Street">
                    <xal:ThoroughfareNumber>2</xal:ThoroughfareNumber>
                    <xal:ThoroughfareName>Marienbongard</xal:ThoroughfareName>
                  </xal:Thoroughfare>
                  <xal:PostalCode>
                    <xal:PostalCodeNumber>52062</xal:PostalCodeNumber>
                  </xal:PostalCode>
                </xal:Locality>
              </xal:Country>
            </xal:AddressDetails>
          </core:xalAddress>
        </core:Address>
      </bldg:address>
    </bldg:Building>
  </core:cityObjectMember>
</core:CityModel>
