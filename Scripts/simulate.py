import os
import pandas as pd
from buildingspy.simulate.Simulator import Simulator
from buildingspy.io.outputfile import Reader
import matplotlib.pyplot as plt
from multiprocessing import Pool
from timeit import default_timer as timer
import re
import shutil


def simulate(path, prj, loading_time, result_path, visualization=False, save_in_gml=False):
    """BuildingsPy: simulation, loading and saving results, visualization
    :param save_in_gml: saves the heatload series in EnergyADE
    :param visualization: Graphs the heatload, inner and outside temperature
    :param result_path: path to the excated heating load Dataframe
    :param loading_time: Initial TEASER+ loasing time
    :param prj: TEASER+ Instance
    :param path: Simulation Result path
    """

    """BuildingsPy: Setup / Simulation"""
    start_simulation = timer()
    li = []
    for buildings in prj.buildings:
        s = Simulator(modelName=prj.name + "." + buildings.name + "." + buildings.name,
                          simulator="dymola", packagePath=path + "\\" + prj.name,
                          outputDirectory=path + "\\" + 'BuildingsPyResults' + "\\" + prj.name + "\\results")
        li.append(s)

    po = Pool()
    po.map(simulateCase, li)

    end_simulation = timer()
    simulation_time = (end_simulation-start_simulation)

    """Clean up"""
    # for buildings in prj1.buildings:
    #     shutil.rmtree(path + "\\" + prj1.name)

    """Area's and Surfaces"""
    df_surfaces = pd.DataFrame(columns=['surface_index'])
    surface_index = ['zone_area', 'zone_volume', 'orientations', 'roof_orientations', 'roof_area',
                     'ground_area', 'interior_wall_area', 'window_area', 'wall_area']
    df_surfaces['surface_index'] = surface_index
    df_surfaces.index = surface_index
    # df_surfaces.set_index(surface_index, inplace=True)

    for buildings in prj.buildings:
        try:
            buildings.net_leased_area
            results = os.path.join(path + "\\" + 'BuildingsPyResults' + "\\" +
                         prj.name + "\\results\\" + buildings.name)
            r = Reader(results, "dymola")

            sim_area, zone_area = r.values('multizone.zoneParam[1].AZone')
            sim_vol, zone_volume = r.values('multizone.zoneParam[1].VAir')
            sim_window, n_orientations = r.values('multizone.zoneParam[1].nOrientations')
            sim_roof, n_orientations_roof = r.values('multizone.zoneParam[1].nOrientationsRoof')
            sim_int, int_wall_area = r.values('multizone.zoneParam[1].AInt')
            sim_roof, roof_area = r.values('multizone.zoneParam[1].ARoof')
            sim_floor, ground_area = r.values('multizone.zoneParam[1].AFloor')

            df_surfaces.at['zone_area', buildings.name] = zone_area[0]
            df_surfaces.at['zone_volume', buildings.name] = zone_volume[0]
            df_surfaces.at['orientations', buildings.name] = n_orientations[0]
            df_surfaces.at['roof_orientations', buildings.name] = n_orientations_roof[0]
            df_surfaces.at['interior_wall_area', buildings.name] = int_wall_area[0]
            df_surfaces.at['roof_area', buildings.name] = roof_area[0]
            df_surfaces.at['ground_area', buildings.name] = ground_area[0]
            df_surfaces.at['net_leased_area', buildings.name] = buildings.net_leased_area

            wall_area_agg = 0
            window_area_agg = 0
            for orient in range(1, int(n_orientations[0] + 1)):
                info, window_area = r.values(f'multizone.zoneParam[1].AWin[{orient}]')
                info, wall_area = r.values(f'multizone.zoneParam[1].AExt[{orient}]')
                wall_area_agg += wall_area[0]
                window_area_agg += window_area[0]
            df_surfaces.at['wall_area', buildings.name] = wall_area_agg
            df_surfaces.at['window_area', buildings.name] = window_area_agg
        except:
            pass

    df_surfaces.to_csv(f'C:/Users/Folder/{prj.name}_surfaces.csv')

    """BuildingsPy: Load Results"""
    df_results = pd.DataFrame()
    start_post_processing = timer()
    for buildings in prj.buildings:
        if re.match(r"(\w+)bt_", buildings.name):
            try:
                matchObj = re.match(r"(\w+)bt_", buildings.name)
                results = os.path.join(path + "\\" + 'BuildingsPyResults' + "\\" +
                                       prj.name + "\\results\\" + buildings.name)
                r = Reader(results, "dymola")
                (time1, heatload) = r.values('multizone.PHeater[1]')
                if matchObj.group(1) in df_results.columns:
                    hl = pd.Series(heatload)
                    df_results[matchObj.group(1)] = df_results[matchObj.group(1)].add(hl)
                else:
                    df_results[matchObj.group(1)] = heatload
            except:
                print(f'failed simulation for building {buildings.name}')
        else:
            try:
                results = os.path.join(path + "\\" + 'BuildingsPyResults' + "\\" +
                                       prj.name + "\\results\\" + buildings.name)
                r = Reader(results, "dymola")
                (time1, heatload) = r.values('multizone.PHeater[1]')

                (time2, insidetemperature) = r.values('multizone.TAir[1]')
                (time3, outsidetemperature) = r.values('weaDat.weaBus.TDryBul')

                """Write in a DataFrame, Each buiding gets a column for the heatload"""
                df_results[f'{buildings.name}'] = heatload

                # df_results.to_csv(f'C:/Users/MaxPaine33/PycharmProjects/teaserplus/gmlfiles/Hamburg/'
                #                   f'Results_pandas_csv/5_Buildings_test/{prj.name}_results.csv')

            except:
                print(f'failed simulation for building {buildings.name}')

    if visualization:
        """BuildingsPy: plot Results"""
        fig = plt.figure(figsize=(12.8, 7.2), frameon=False, dpi=150)
        ax = fig.add_subplot(211)
        ax.plot(time1 / 3600, heatload / 1000, 'b', label=f'{buildings.name}', linewidth=1)
        ax.set_xlabel('Time [h]')
        ax.set_ylabel('Heat load [kW]')
        ax.xaxis.get_major_ticks(1440)
        ax.set_xlim(0, 8760)
        ax.legend()

        ax = fig.add_subplot(212)
        ax.plot(time3 / 3600, outsidetemperature - 273.15, 'b', label='Outside Temperature', linewidth=1)
        ax.plot(time2 / 3600, insidetemperature - 273.15, 'r', label='Inside Temperature', linewidth=1)
        ax.set_xlabel('Time [h]')
        ax.set_ylabel('temperature [$^\circ$C]')
        ax.xaxis.get_major_ticks(1440)
        ax.set_xlim(0, 8760)
        ax.legend()
        plt.show()

    '''calculating the simulation time'''
    end_post_processing = timer()
    post_processing_time = (end_post_processing-start_post_processing)
    df_results['loading time'] = loading_time
    df_results['simulation time'] = simulation_time
    df_results['post processing time'] = post_processing_time

    df_results.to_csv(result_path)

    if save_in_gml:
        prj.save_citygml(path="/gmlfiles/ADE out", results=heatload)


def simulateCase(s):
    """
    sets the general parameters for the simulation
    :param s: BuildingsPy Simulator object
    """
    s.showGUI(show=False)
    # s.translate()
    s.setSolver("Cvode")
    print(s.printModelAndTime())
    s.setStartTime(0)
    s.setStopTime(31536000)
    s.setNumberOfIntervals(8760)
    s.simulate()