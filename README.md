# TEASERplus

Based on [TEASER](https://github.com/RWTH-EBC/TEASER) version 0.6.9. This extension builds on the base archetype to Modelica Model
creation capabilities of TEASER and enhances as well as expands on the input options for CityGML Buildings. CityGML is a XML based
3D city Model language which can hold rudimentary geometric data together with semantic information. 
## Version

This version of Teaser+ (v.01) has the functionality to import CityGML (LoD0 - Lod4) with and without the Energy ADE. 
The data models can be further exported as CityGML + Energy ADE. 
The extended data model will contain the enrichment information complying to the EnergyADE v1.0 schema.

## Dependencies
TEASERplus is only tested against Python version 3.6 and may not run on other versions.
Beside typical libraries (NumPy, SciPy, Matplotlib,Pandas etc.) that are usually already included in Python distributions like Anaconda 
or WinPython, TEASERplus requires the libraries:

* [Mako](https://www.makotemplates.org/): A template Engine used for Python to Modelica Model templates
* [BuildingsPy](https://simulationresearch.lbl.gov/modelica/buildingspy/): Used for running the simulations directly in python and extract results
* [PyXB](https://pypi.org/project/PyXB/): Needed for bindings of XML to Python Classes. Although the library is no longer maintained, because of it vital part for CityGML input
it is still needed.

## How to use TEASERplus
For the moment the installation process is only enabled by `git clone [SSH-Key/Https]`. 

Example scripts are provided in the `Scripts`Scripts directory.

## How to cite TEASERplus
* A. Malhotra, M. Shamovich, J. Frisch, C. van Treeck, Parametric study of the different level of detail of citygml and energy-ade informationfor energy performance simulations, 2019, pp. 3429–3436.doi:10.26868/25222708.2019.210607

## License
CityATB is released by RWTH Aachen University, E3D - Institute of Energy Efficiency and Sustainable Building, under the [MIT License](/LICENSE).