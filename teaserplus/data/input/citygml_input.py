# Created April 2016
# TEASER Development Team

"""CityGML

This module contains function to load Buildings in the non proprietary
CityGML file format .gml
"""
import numpy as np
from numpy import linalg as LA
import collections
import pyxb
import pyxb.utils
import pyxb.namespace
import pyxb.bundles
import pyxb.bundles.common.raw.xlink as xlink
import teaserplus.data.bindings.opengis
import teaserplus.data.bindings.opengis.citygml.raw.base as citygml
import teaserplus.data.bindings.opengis.citygml.raw.energy as energy
import teaserplus.data.bindings.opengis.citygml.raw.building as gmlbldg
import teaserplus.data.bindings.opengis.raw.gml as gml
import teaserplus.data.bindings.opengis.citygml.raw.generics as gen
import teaserplus.data.bindings.opengis.raw._nsgroup as nsgroup
import teaserplus.data.bindings.opengis.raw.smil20 as smil
import teaserplus.data.bindings.opengis.misc.raw.xAL as xal
# import teaserplus.project as prj
from teaserplus.data.dataclass import DataClass
from teaserplus.logic.archetypebuildings.bmvbs.singlefamilydwelling \
                         import SingleFamilyDwelling
from teaserplus.logic.archetypebuildings.bmvbs.office import Office
from teaserplus.logic.archetypebuildings.bmvbs.custom.institute import Institute
from teaserplus.logic.archetypebuildings.bmvbs.custom.institute4 import Institute4
from teaserplus.logic.archetypebuildings.tabula.de.singlefamilyhouse import SingleFamilyHouse
from teaserplus.logic.archetypebuildings.tabula.de.multifamilyhouse import MultiFamilyHouse
from teaserplus.logic.archetypebuildings.tabula.de.terracedhouse import TerracedHouse
from teaserplus.logic.archetypebuildings.tabula.de.apartmentblock import ApartmentBlock
from teaserplus.logic.buildingobjects.building import Building
from teaserplus.logic.buildingobjects.thermalzone import ThermalZone
from teaserplus.logic.buildingobjects.buildingphysics.rooftop import Rooftop
from teaserplus.logic.buildingobjects.buildingphysics.outerwall import OuterWall
from teaserplus.logic.buildingobjects.buildingphysics.groundfloor import GroundFloor
from teaserplus.logic.buildingobjects.buildingphysics.layer import Layer
from teaserplus.logic.buildingobjects.buildingphysics.material import Material
from teaserplus.logic.buildingobjects.buildingphysics.buildingelement import BuildingElement
from teaserplus.logic.buildingobjects.buildingphysics.window import Window
from teaserplus.logic.buildingobjects.buildingphysics.innerwall import InnerWall
from teaserplus.logic.buildingobjects.buildingphysics.ceiling import Ceiling
from teaserplus.logic.buildingobjects.buildingphysics.floor import Floor
from teaserplus.logic.buildingobjects.buildingphysics.door import Door
from teaserplus.logic.buildingobjects.boundaryconditions.boundaryconditions import BoundaryConditions
import copy


def choose_gml(path, prj, bldg_ids=None, bldg_names=None, bldg_addresses=None):
    """This function loads buildings from a CityGML file and
        selects specific buildings by Id, name ore address

        This function is a proof of concept, be careful using it.

        Parameters
        ----------
        path: string
            path of CityGML file
        prj: Project()
            Teaser instance of Project()
        bldg_addresses: list[string+string]
            users choice
        bldg_names: list[string]
            users choice
        bldg_ids: list[string]
            users choice

        """

    xml_file = open(path, 'r')
    gml_bind = citygml.CreateFromDocument(xml_file.read())

    chosen_gmls = []

    if bldg_ids is not None:
        for i, city_object in enumerate(gml_bind.featureMember):
            if city_object.Feature.id in bldg_ids:
                chosen_gmls.append(copy.copy(city_object))

    elif bldg_names is not None:
        for i, city_object in enumerate(gml_bind.featureMember):
            # print(city_object.Feature.name[0].value())
            print(city_object.Feature.externalReference[0].externalObject.name)
            # if city_object.Feature.name[0].value() in bldg_names \
            #         or city_object.Feature.externalReference[0].externalObject.name in bldg_names:
            if city_object.Feature.externalReference[0].externalObject.name in bldg_names:
                chosen_gmls.append(copy.copy(city_object))
            else:
                print(f'{city_object.Feature.externalReference[0].externalObject.name} not in selected Buildings')
    else:
        pass
    return chosen_gmls


def load_gml(path, prj, method, chosen_gmls=None, yoc_list=None):
    """This function loads buildings from a CityGML file

    This function is a proof of concept, be careful using it.

    Parameters
    ----------
    path: string
        path of CityGML file

    prj: Project()
        Teaser instance of Project()

    :param method: Str
            method for enrichment of single family dwellings
            either default="iwu" or "tabula_de"
            offices always use "iwu"= BMVBS and other residential
            buildings will be always using "tabula_de"

    chosen_gmls: List[of chosen CityObject(Buildings)]
    """

    if chosen_gmls is None:
        with open(path, 'r') as xml_file:
            gml_bind = citygml.CreateFromDocument(xml_file.read())
            featureMembers = enumerate(gml_bind.featureMember)
    else:
        featureMembers = enumerate(chosen_gmls)

    gml_copy_list = []

    for i, city_object in featureMembers:
        gml_copy_list.append(copy.copy(city_object))

        if city_object.Feature.consistsOfBuildingPart:
            for e, part in enumerate(city_object.Feature.consistsOfBuildingPart):
                if city_object.Feature.function[0].value() == "31001_1000" \
                     or city_object.Feature.function[0].value() == "31001_1010":
                        # or part.BuildingPart.function[0].value() == "31001_1000" \
                        # or part.BuildingPart.function[0].value() == "31001_1010" \
                        # or part.BuildingPart.function[0].value() == "1000":
                    if method == "tabula_de":
                        prj.data = DataClass(used_statistic="tabula_de")
                        bldg = SingleFamilyHouse(parent=prj,
                                                name=part.BuildingPart.id)
                    else:
                        bldg = SingleFamilyDwelling(parent=prj,
                                                    name=part.BuildingPart.id)
                elif city_object.Feature.function[0].value() == "31001_2020"\
                        or city_object.Feature.function[0].value() == "31001_3023"\
                        or city_object.Feature.function[0].value() == "31001_3024":
                # elif part.BuildingPart.function[0].value() == "1120" \
                #         or part.BuildingPart.function[0].value() == "31001_3000" \
                #         or city_object.Feature.function[0].value() == "31001_2023":
                    bldg = Institute(parent=prj,
                                  name=part.BuildingPart.id)
                else:
                    prj.data = DataClass(used_statistic='iwu')
                    bldg = Building(parent=prj,
                                    name=part.BuildingPart.id)

                _create_building_part(bldg=bldg, part=part)
                # _set_attributes(bldg=bldg, gml_bldg=part.BuildingPart)
                if yoc_list is not None:
                    _set_attributes(bldg=bldg, gml_bldg=city_object.Feature, gml_bldg_part=part.BuildingPart,
                                    bldg_part=e, bldg_yoc=yoc_list[i])
                else:
                    _set_attributes(bldg=bldg, gml_bldg=city_object.Feature,
                                    gml_bldg_part=part.BuildingPart, bldg_part=e)

                bldg.set_height_gml()
                try:
                    bldg.set_gml_attributes()
                except (UserWarning, AttributeError, Exception):
                    print(f"{bldg.name} bldg.set_gml_attributes() did not work")
                    pass
                try:
                    bldg.generate_from_gml()
                except (UserWarning, AttributeError, Exception):
                    print(f"{bldg.name} bldg.generate_from_gml() did not work")
                    pass

        else:
            if city_object.Feature.function:
                if city_object.Feature.function[0].value() == "1000" \
                        or city_object.Feature.function[0].value() == "31001_1010" \
                        or city_object.Feature.function[0].value() == "31001_2024":
                    if method == "tabula_de":
                        prj.data = DataClass(used_statistic="tabula_de")
                        bldg = SingleFamilyHouse(parent=prj, name=city_object.Feature.id)
                    else:
                        prj.data = DataClass(used_statistic='iwu')
                        bldg = SingleFamilyDwelling(parent=prj, name=city_object.Feature.id)
                elif city_object.Feature.function[0].value() == "1120" \
                        or city_object.Feature.function[0].value() == "31001_3000" \
                        or city_object.Feature.function[0].value() == "31001_2020" \
                        or city_object.Feature.function[0].value() == "31001_3023" \
                        or city_object.Feature.function[0].value() == "31001_1120" \
                        or city_object.Feature.function[0].value() == "3023" \
                        or city_object.Feature.function[0].value() == "31001_3024":
                    prj.data = DataClass(used_statistic='iwu')
                    bldg = Institute(parent=prj,
                                  name=city_object.Feature.id)
                else:
                    bldg = Building(parent=prj,
                                    name=city_object.Feature.id)
            else:
                bldg = Building(parent=prj,
                                name=city_object.Feature.id)

            _create_building(bldg=bldg, city_object=city_object)
            if yoc_list is not None:
                _set_attributes(bldg=bldg, gml_bldg=city_object.Feature, bldg_yoc=yoc_list[i])
            else:
                _set_attributes(bldg=bldg, gml_bldg=city_object.Feature)
            bldg.set_height_gml()
            try:
                bldg.set_gml_attributes()
            except (UserWarning, AttributeError):
                print(f"{bldg.name} bldg.set_gml_attributes() did not work")
                pass
            try:
                bldg.generate_from_gml()
            except (UserWarning, AttributeError):
                print(f"{bldg.name} bldg.generate_from_gml() did not work")
                pass

    return gml_copy_list


def load_gmlade(path, prj, chosen_gmls=None):
    """This function loads buildings from a CityGML EnergyADE files

        This function is a proof of concept, be careful using it.

        Parameters
        ----------
        path: string
            path of CityGML file

        prj: Project()
            Teaser instance of Project()

        chosen_gmls: List[of chosen CityObject(Buildings)]

        tzb_dict: python dictionary containing the ThermalBoudary information
            {tzb_gid:[type, azimuth, inclination, area, construction_gid]}

        tzb_dict_openings: python dictionary containing the ThermalBoudaryOpening information
            {tzb_opening_gid:[tzb_gid, area inclination, azimuth, construction_gid]}

        layer_dict: python dictionary containing the layer component information
            {construction_gid:[layer_gid, layer_component_gid, layer_thickness, material_gid]}
            {construction_gid:[name, ,U-value, glazing_ratio, fraction]}

        material_dict: python dictionary containing the Material information
            {material_gid:[name, density, conductivity, heat capacity]}
        """
    if chosen_gmls is None:
        with open(path, 'r') as xml_file:
            gml_bind = citygml.CreateFromDocument(xml_file.read())
            featureMembers = gml_bind.featureMember
    else:
        # TODO: Won't work for multiple EnergyADE buildings (IDEA: save CityObjects_Building separate)
        featureMembers = enumerate(chosen_gmls)

    """Envelope Upper/Lower Corner"""

    # lowercorner = gml_bind.boundedBy.Envelope.lowerCorner.value()
    # uppercorner = gml_bind.boundedBy.Envelope.upperCorner.value()
    # print(f'The Lower Corner:{lowerorner} and Upper Corner:{uppercorner} of the City Object')

    """collects all Construction - and Materialtypes from the EnergyADE file"""

    layer_dict, material_dict, constr_dict = get_layers_materials_dicts(featureMembers=featureMembers)
    for i, city_object in enumerate(featureMembers):
        if isinstance(city_object.Feature, gmlbldg.BuildingType):
            bldg = Building(parent=prj)
            _set_attributes(bldg, gml_bldg=city_object.Feature)
            bldg.type_of_building = get_ade_usage_zone_type(city_object.Feature)
            bldgsizetype, volume, tz_id, tz_infiltration_rate, cooled, heated = \
                get_ade_generalinfo(generalinfo=city_object.Feature)

            """Trying to set ThermalZone"""
            # tz = ThermalZone(parent=bldg)
            # tz.name = tz_id
            # tz.area = get_ade_floor_area(city_object.Feature)
            # tz.volume = volume
            # if tz_infiltration_rate is not None:
            #     tz.infiltration_rate = tz_infiltration_rate
            # else:
            #     pass
            # tzb_dict, tzb_dict_openings = get_ade_thermal_boundaries(city_object.Feature)
            # schedule_heating, schedule_ventilation, schedule_person, schedule_machine, schedule_lighting, \
            # total_value_lighting, convective_fraction_lighting, radiant_fraction_lighting, number_of_occupants, \
            # convective_fraction_persons, radiant_fraction_persons, total_value_machines, convective_fraction_machines, \
            # radiant_fraction_machines = get_ade_schedules(city_object.Feature)
            #
            # set_ade_bldgelements(tz, tzb_dict, tzb_dict_openings, layer_dict, material_dict, constr_dict)
            # set_ade_boundary_conditions(prj, tz, tz_id, bldg.type_of_building, tz.area,
            #                             schedule_heating, schedule_ventilation, schedule_person, schedule_machine,
            #                             schedule_lighting, total_value_lighting, convective_fraction_lighting,
            #                             radiant_fraction_lighting, number_of_occupants, convective_fraction_persons,
            #                             radiant_fraction_persons, total_value_machines, convective_fraction_machines,
            #                             radiant_fraction_machines)
            # # TODO: Write separate function, including the case of Interior Walls already in EnergyADE
            # """Setting Inner Walls"""
            # floor = Floor(parent=tz)
            # floor.name = "floor"
            # floor.tilt = 0
            # floor.load_type_element(year=bldg.year_of_construction, construction='heavy')
            #
            # ceiling = Ceiling(parent=tz)
            # ceiling.name = "ceiling"
            # ceiling.tilt = 180
            # ceiling.load_type_element(year=bldg.year_of_construction, construction='heavy')
            #
            # inner = InnerWall(parent=tz)
            # inner.name = "innerWall"
            # inner.tilt = 90
            # inner.load_type_element(year=bldg.year_of_construction, construction='heavy')
            # tz.set_inner_wall_area()

            """Office building MultiZone model test"""
            zone_area_factors = collections.OrderedDict()
            zone_area_factors["Office"] = \
                [0.5, "Group Office (between 2 and 6 employees)"]
            zone_area_factors["Floor"] = \
                [0.25, "Traffic area"]
            zone_area_factors["Storage"] = \
                [0.15, "Stock, technical equipment, archives"]
            zone_area_factors["Meeting"] = \
                [0.04, "Meeting, Conference, seminar"]
            zone_area_factors["Restroom"] = \
                [0.04, "WC and sanitary rooms in non-residential buildings"]
            zone_area_factors["ICT"] = \
                [0.02, "Data center"]

            for key, value in zone_area_factors.items():
                print(key)
                if key == 'Office':

                    """Trying to set ThermalZone"""
                    tz = ThermalZone(parent=bldg)
                    tz.name = tz_id
                    tz.area = get_ade_floor_area(city_object.Feature) * value[0]
                    tz.volume = volume
                    if tz_infiltration_rate is not None:
                        tz.infiltration_rate = tz_infiltration_rate
                    else:
                        pass
                    tzb_dict, tzb_dict_openings = get_ade_thermal_boundaries(city_object.Feature)
                    schedule_heating, schedule_ventilation, schedule_person, schedule_machine, schedule_lighting, \
                    total_value_lighting, convective_fraction_lighting, radiant_fraction_lighting, number_of_occupants, \
                    convective_fraction_persons, radiant_fraction_persons, total_value_machines, convective_fraction_machines, \
                    radiant_fraction_machines = get_ade_schedules(city_object.Feature)

                    set_ade_bldgelements(tz, tzb_dict, tzb_dict_openings, layer_dict, material_dict, constr_dict, value[0])
                    set_ade_boundary_conditions(prj, tz, tz_id, bldg.type_of_building, tz.area,
                                                schedule_heating, schedule_ventilation, schedule_person, schedule_machine,
                                                schedule_lighting, total_value_lighting, convective_fraction_lighting,
                                                radiant_fraction_lighting, number_of_occupants,convective_fraction_persons,
                                                radiant_fraction_persons, total_value_machines, convective_fraction_machines,
                                                radiant_fraction_machines)

                    # TODO: Write separate function, including the case of Interior Walls already in EnergyADE
                    """Setting Inner Walls"""
                    floor = Floor(parent=tz)
                    floor.name = "floor"
                    floor.tilt = 0
                    floor.load_type_element(year=bldg.year_of_construction, construction='heavy')

                    ceiling = Ceiling(parent=tz)
                    ceiling.name = "ceiling"
                    ceiling.tilt = 180
                    ceiling.load_type_element(year=bldg.year_of_construction, construction='heavy')

                    inner = InnerWall(parent=tz)
                    inner.name = "innerWall"
                    inner.tilt = 90
                    inner.load_type_element(year=bldg.year_of_construction, construction='heavy')
                    tz.set_inner_wall_area()

                else:
                    tz = ThermalZone(parent=bldg)
                    tz.area = get_ade_floor_area(city_object.Feature) * value[0]
                    tz.name = key
                    tz.volume = volume * value[0]
                    tz.use_conditions = BoundaryConditions(parent=tz)
                    tz.use_conditions.load_use_conditions(value[1], prj.data)
                    tz.use_conditions.with_ahu = False
                    tz.use_conditions.persons *= tz.area * 0.01
                    tz.use_conditions.machines *= tz.area * 0.01
                    tzb_dict, tzb_dict_openings = get_ade_thermal_boundaries(city_object.Feature)
                    set_ade_bldgelements(tz, tzb_dict, tzb_dict_openings, layer_dict, material_dict, constr_dict, value[0])

                    # TODO: Write separate function, including the case of Interior Walls already in EnergyADE
                    """Setting Inner Walls"""
                    floor = Floor(parent=tz)
                    floor.name = "floor"
                    floor.tilt = 0
                    floor.load_type_element(year=bldg.year_of_construction, construction='heavy')

                    ceiling = Ceiling(parent=tz)
                    ceiling.name = "ceiling"
                    ceiling.tilt = 180
                    ceiling.load_type_element(year=bldg.year_of_construction, construction='heavy')

                    inner = InnerWall(parent=tz)
                    inner.name = "innerWall"
                    inner.tilt = 90
                    inner.load_type_element(year=bldg.year_of_construction, construction='heavy')
                    tz.set_inner_wall_area()


def get_layers_materials_dicts(featureMembers):
    """Total U-Values for BuildingPart(Außenwand,Dach,..) / Total parameter for Windows / Layers and Components"""
    layer_dict = {}
    material_dict = {}
    constr_dict = {}  # specifically for the ConvectionCoefficients for EnergyADE v2
    # TODO: Find nice solution for the constr_dict and it's coefficents
    for i, member in enumerate(featureMembers):
        try:
            constr_gid = member.Feature.id
            name = member.Feature.name[0].value()
            uvalue = member.Feature.uValue.value()
            layer_dict[constr_gid] = []
            constr_dict[constr_gid] = []  # specifically for the ConvectionCoefficients for EnergyADE v2
            # description = constr_member.Feature.description.value()
            # uom = constr_member.Feature.uValue.uom
            if member.Feature.opticalProperties is not None:
                # new energyADE v2
                # if member.Feature.insideConvectionCoefficient:
                #     if member.Feature.outsideConvectionCoefficient:
                #         outcc = member.Feature.outsideConvectionCoefficient.value()
                #     else:
                #         outcc = None
                #     incc = member.Feature.insideConvectionCoefficient.value()
                #     constr_dict[constr_gid].append([outcc, incc])
                # else:
                #     pass
                glazingratio = member.Feature.opticalProperties.OpticalProperties.glazingRatio.value()
                # glazingratio_uom = constr_member.Feature.opticalProperties.OpticalProperties.glazingRatio.uom
                for i, transmittance in enumerate(
                        member.Feature.opticalProperties.OpticalProperties.transmittance):
                    fraction = transmittance.Transmittance.fraction.value()
                    layer_dict[constr_gid].append([name, uvalue, glazingratio, fraction])
                    # fraction_uom = transmittance.Transmittance.fraction.uom
            else:
                # new energyADE v2
                # if member.Feature.insideConvectionCoefficient:
                #     print(member.Feature.insideConvectionCoefficient.value())
                #     if member.Feature.outsideConvectionCoefficient:
                #         outcc = member.Feature.outsideConvectionCoefficient.value()
                #     else:
                #         outcc = None
                #     incc = member.Feature.insideConvectionCoefficient.value()
                #     constr_dict[constr_gid].append([outcc, incc])
                # else:
                #     pass
                for i, layer in enumerate(member.Feature.layer):
                    # l_thickness = layer.Layer.thickness.value() # new EnergyADE v2
                    l_gid = layer.Layer.id
                    for e, l_comp in enumerate(layer.Layer.layerComponent):
                        l_comp_gid = l_comp.LayerComponent.id
                        l_thickness = l_comp.LayerComponent.thickness.value()
                        material_href = l_comp.LayerComponent.material.href.strip('#')
                        layer_dict[constr_gid].append([l_gid, l_comp_gid, l_thickness, material_href])
                        # l_areafraction = l_comp.LayerComponent.areaFraction.value()
                        # l_areafraction_uom = l_comp.LayerComponent.areaFraction.uom
                        # l_thickness_uom = l_comp.LayerComponent.thickness.uom
        except (AttributeError, IndexError):
            pass
        try:
            material_name = member.Feature.name[0].value()
            material_gid = member.Feature.id
            conductivity = member.Feature.conductivity.value()
            density = member.Feature.density.value()
            heat_capac = member.Feature.specificHeat.value()
            material_dict[material_gid] = []
            material_dict[material_gid].append([material_name, density, conductivity, heat_capac])
            # description = material.Feature.description.value()
            # heat_capac_uom = material.Feature.specificHeat.uom
            # density_uom = material.Feature.density.uom
            # conductivity_uom = material.Feature.conductivity.uom
        except (AttributeError, IndexError):
            pass
        try:
            material_name = member.Feature.name[0].value()
            material_gid = member.Feature.id
            rValue = member.Feature.rValue.value()
            isventilated = member.Feature.isVentilated
            material_dict[material_gid] = []
            material_dict[material_gid].append([material_name, rValue, isventilated])
            # description = gas.Feature.description.value()
            # rValue_uom = gas.Feature.rValue.uom
        except (AttributeError, IndexError):
            pass

    return layer_dict, material_dict, constr_dict


def get_ade_generalinfo(generalinfo):
    """General ThermalZone/building Info: ConstructionWeight, VolumeType, Volume, Referencepoints,
                ThermalZoneType/ID/Hrefs, Heated/Cooled"""

    bldgsizetype = "Not available"
    for i, info in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
        if isinstance(info, gml.CodeType):
            bldgsizetype = info.value()
        elif isinstance(info, energy.VolumeTypePropertyType):
            volume = info.VolumeType.value_.value()
            volume_uom = info.VolumeType.value_.uom
        elif isinstance(info, gml.PointPropertyType):
            srsname = info.Point.srsName
            pointpos = info.Point.pos.value()
        elif isinstance(info, energy.AbstractThermalZonePropertyType):
            tz_id = info.AbstractThermalZone.id
            try:
                tz_infiltration_rate = info.AbstractThermalZone.infiltrationRate.value()
            except AttributeError:
                tz_infiltration_rate = None
            # tz_type = info.type
            # tz_href = info.AbstractThermalZone.contains[0].href
            # tz_srsname = info.AbstractThermalZone.boundedBy.Envelope.srsName
            # tz_lcorner = info.AbstractThermalZone.boundedBy.Envelope.lowerCorner.value()
            # tz_ucorner = info.AbstractThermalZone.boundedBy.Envelope.upperCorner.value()
            cooled = info.AbstractThermalZone.isCooled
            heated = info.AbstractThermalZone.isHeated
            if cooled is False:
                print('The Building is not cooled')
            else:
                print('The Building is cooled')
            if heated is False:
                print('The Building is not heated')
            else:
                print('The Building is heated')
        elif isinstance(info, str):
            constr_weight = info
    # TODO: Is the construction weight used anywhere in TEASER? Otherwise leave it out
    """ThermalZone Geometry: SolidId, CompositeSurfaceId and the surfaceMember hrefs"""

    # for i, info in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
    #     if isinstance(info, energy.AbstractThermalZonePropertyType):
    #         solid_id = info.AbstractThermalZone.volumeGeometry.Solid.id
    #         surface_id = info.AbstractThermalZone.volumeGeometry.Solid.exterior.Surface.id
    #         for v, surfacemember in enumerate(info.AbstractThermalZone.volumeGeometry.
    #                                           Solid.exterior.Surface.surfaceMember):
    #             surfacemember_href = surfacemember.href

    return bldgsizetype, volume, tz_id, tz_infiltration_rate, cooled, heated


def get_ade_thermal_boundaries(generalinfo):
    """ThermalBoundaries and ThermalOpenings"""
    tzb_dict = {}
    tzb_dict_openings = {}

    for v, thermalbound in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
        if isinstance(thermalbound, energy.AbstractThermalZonePropertyType):
            tzb_boundedby = thermalbound.AbstractThermalZone.boundedBy_
            for i, bounded_object in enumerate(tzb_boundedby):
                tzb_type = bounded_object.ThermalBoundary.thermalBoundaryType
                tzb_gid = bounded_object.ThermalBoundary.id
                tzb_azimuth = bounded_object.ThermalBoundary.azimuth.value()
                tzb_inclination = bounded_object.ThermalBoundary.inclination.value()
                tzb_area = bounded_object.ThermalBoundary.area.value()
                tzb_constr_href = bounded_object.ThermalBoundary.construction.href.strip('#')
                # tzb_delimits_href = bounded_object.ThermalBoundary.delimits[0].href.strip('#') old EnergyADE v1
                # tzb_isAdjacentTo_href = bounded_object.ThermalBoundary.isAdjacentTo[0]
                #                           .href.strip('#') new EnergyADE v2
                # tzb_lcorner = bounded_object.ThermalBoundary.boundedBy.Envelope.lowerCorner.value()
                # tzb_ucorner = bounded_object.ThermalBoundary.boundedBy.Envelope.upperCorner.value()
                # tzb_azimuth_uom = bounded_object.ThermalBoundary.azimuth.uom
                # tzb_inclination_uom = bounded_object.ThermalBoundary.inclination.uom
                # tzb_area_uom = bounded_object.ThermalBoundary.area.uom
                tzb_dict.update({tzb_gid: [tzb_type, tzb_azimuth, tzb_inclination, tzb_area, tzb_constr_href]})
                print(tzb_dict)
                try:
                    for openings in bounded_object.ThermalBoundary.contains:
                        tzb_contains_tho_id = openings.ThermalOpening.id
                        tzb_contains_tho_area = openings.ThermalOpening.area.value()
                        tzb_contains_tho_area_uom = openings.ThermalOpening.area.uom
                        tzb_contains_tho_contr_href = openings.ThermalOpening. \
                            construction.href.strip('#')

                        tzb_dict_openings.update({tzb_contains_tho_id: [tzb_gid, tzb_contains_tho_area,
                                                                        tzb_inclination, tzb_azimuth,
                                                                        tzb_contains_tho_contr_href]})
                except IndexError:
                    continue
        else:
            continue

    return tzb_dict, tzb_dict_openings

    # TODO ThermalBoundary: SurfaceGeometry could be extracted from here or checked
    # tzb_geometrie = tzb_boundedby[0].ThermalBoundary.surfaceGeometry
    return tzb_dict, tzb_dict_openings


def get_ade_usage_zone_type(generalinfo):
    for b, usage_zone in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
        if isinstance(usage_zone, energy.AbstractUsageZonePropertyType):
            type_of_usage = usage_zone.AbstractUsageZone.usageZoneType.value()
            break
    return type_of_usage


def get_ade_schedules(generalinfo):
    """UsageZone: heating/ventilation/occupants and equipment schedules"""
    for b, usage_zone in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
        if isinstance(usage_zone, energy.AbstractUsageZonePropertyType):
            usage = usage_zone

    try:
        period_heating = usage.AbstractUsageZone.heatingSchedule.AbstractSchedule
        period_heating.internal_id = "heating"
    except (AttributeError, IndexError):
        period_heating = None
        pass
    try:
        period_cooling = usage.AbstractUsageZone.coolingSchedule.AbstractSchedule
        period_cooling.internal_id = "heating"
    except (AttributeError, IndexError):
        period_cooling = None
        pass
    try:
        period_ventilation = usage.AbstractUsageZone.ventilationSchedule.AbstractSchedule
        period_ventilation.internal_id = "ventilation"
    except (AttributeError, IndexError):
        period_ventilation = None
        pass
    try:
        period_occupancy = usage.AbstractUsageZone.occupiedBy[0].Occupants.occupancyRate.AbstractSchedule
        period_occupancy.internal_id = "occupancy"
    except (AttributeError, IndexError):
        period_occupancy = None
        pass
    """Occupants/ Equippedwith/ Heating/ Cooling"""
    try:
        for i, occupants in enumerate(usage.AbstractUsageZone.occupiedBy):
            # gid = occupants.Occupants.id
            number_of_occupants = occupants.Occupants.numberOfOccupants
            print(occupants.Occupants.numberOfOccupants)
            convective_fraction_persons = occupants.Occupants.heatDissipation. \
                HeatExchangeType.convectiveFraction.value()
            convective_fraction_uom = occupants.Occupants.heatDissipation. \
                HeatExchangeType.convectiveFraction.uom
            radiant_fraction_persons = occupants.Occupants.heatDissipation. \
                HeatExchangeType.radiantFraction.value()
            radiant_fraction_uom = occupants.Occupants.heatDissipation. \
                HeatExchangeType.radiantFraction.uom
            total_value_persons = occupants.Occupants.heatDissipation. \
                HeatExchangeType.totalValue.value()
            total_value_uom = occupants.Occupants.heatDissipation. \
                HeatExchangeType.totalValue.uom
    except (AttributeError, IndexError):
        number_of_occupants = 0
        convective_fraction_persons = 0
        radiant_fraction_persons = 0
        pass
    try:
        for i, facilities in enumerate(usage.AbstractUsageZone.equippedWith):
            name = facilities.Facilities.operationSchedule.AbstractSchedule.name[0].value()
            gid = facilities.Facilities.id
            convective_fraction_facilities = facilities.Facilities.heatDissipation. \
                HeatExchangeType.convectiveFraction.value()
            convective_fraction_uom = facilities.Facilities.heatDissipation. \
                HeatExchangeType.convectiveFraction.uom
            radiant_fraction_facilities = facilities.Facilities.heatDissipation. \
                HeatExchangeType.radiantFraction.value()
            radiant_fraction_uom = facilities.Facilities.heatDissipation. \
                HeatExchangeType.radiantFraction.uom
            total_value_facilities = facilities.Facilities.heatDissipation. \
                HeatExchangeType.totalValue.value()
            total_value_uom = facilities.Facilities.heatDissipation. \
                HeatExchangeType.totalValue.uom

            if isinstance(facilities.Facilities, energy.ElectricalAppliancesType):
                convective_fraction_machines = convective_fraction_facilities
                radiant_fraction_machines = radiant_fraction_facilities
                total_value_machines = total_value_facilities
                try:
                    period_machine = facilities.Facilities.operationSchedule.AbstractSchedule
                    period_machine.internal_id = "machine"
                except (AttributeError, IndexError):
                    period_machine = None
                    pass

            elif isinstance(facilities.Facilities, energy.LightingFacilitiesType):
                convective_fraction_lighting = convective_fraction_facilities
                radiant_fraction_lighting = radiant_fraction_facilities
                total_value_lighting = total_value_facilities
                try:
                    period_lighting = facilities.Facilities.operationSchedule.AbstractSchedule
                    period_lighting.internal_id = "lighting"
                except (AttributeError, IndexError):
                    period_lighting = None
                    pass
            else:
                continue
    except (AttributeError, IndexError):
        # period_lighting = None
        convective_fraction_lighting = 0
        radiant_fraction_lighting = 0
        total_value_lighting = 0

        # period_machine = None
        convective_fraction_machines = 0
        radiant_fraction_machines = 0
        total_value_machines = 0
        pass

    """Schedule loop/List: Names, id, type... """

    schedule_person = []
    schedule_machine = []
    schedule_lighting = []
    schedule_heating = []
    schedule_ventilation = []
    schedule_cooling = []
    period_of_year = {}

    for i in (period_occupancy, period_heating, period_cooling,
              period_ventilation, period_machine, period_lighting):
        if i is not None:
            period_of_year[str(i.internal_id)] = i

    for internal_id, period in period_of_year.items():
        # name = period.name[0].value()

        # begin = period.periodOfYear[0].PeriodOfYear.period.TimePeriod.beginPosition.value()
        # end = period.periodOfYear[0].PeriodOfYear.period.TimePeriod.endPosition.value()
        #
        # weekday_schedule_type = period.periodOfYear[0].PeriodOfYear.dailySchedule[0].DailySchedule.dayType
        # weekend_schedule_type = period.periodOfYear[0].PeriodOfYear.dailySchedule[1].DailySchedule.dayType
        #
        # weekday_schedule_acq_methode = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.acquisitionMethod
        # weekend_schedule_acq_methode = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.acquisitionMethod
        #
        # weekday_schedule_interpolation = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.interpolationType
        # weekend_schedule_interpolation = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.interpolationType
        #
        # weekday_schedule_description = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.thematicDescription
        # weekend_schedule_description = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.variableProperties.TimeValuesProperties.thematicDescription
        #
        # weekday_schedule_begin = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.temporalExtent.TimePeriod.beginPosition.value()
        # weekend_schedule_begin = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.temporalExtent.TimePeriod.beginPosition.value()
        # weekday_schedule_end = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.temporalExtent.TimePeriod.endPosition.value()
        # weekend_schedule_end = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.temporalExtent.TimePeriod.endPosition.value()

        weekday_schedule_temp = period.periodOfYear[0].PeriodOfYear.dailySchedule[
            0].DailySchedule.schedule.AbstractTimeSeries.values.value()
        # weekday_schedule_uom = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.values.uom
        # weekend_schedule_temp = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.values.value()
        # weekend_schedule_uom = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.values.uom
        # weekday_schedule_intervall = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.timeInterval.value()
        # weekday_schedule_intervall_unit = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     0].DailySchedule.schedule.AbstractTimeSeries.timeInterval.unit
        # weekend_schedule_intervall = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.timeInterval.value()
        # weekend_schedule_intervall_unit = period.periodOfYear[0].PeriodOfYear.dailySchedule[
        #     1].DailySchedule.schedule.AbstractTimeSeries.timeInterval.unit

        if internal_id == "heating":
            schedule_heating = weekday_schedule_temp
        elif internal_id == "cooling":
            schedule_cooling = weekday_schedule_temp
        elif internal_id == "ventilation":
            schedule_ventilation = weekday_schedule_temp
        elif internal_id == "occupancy":
            schedule_person = weekday_schedule_temp
        elif internal_id == "machine":
            schedule_machine = weekday_schedule_temp
        elif internal_id == "lighting":
            schedule_lighting = weekday_schedule_temp
    print(schedule_heating)
    print(schedule_cooling)
    print(schedule_person)
    return schedule_heating, schedule_ventilation, schedule_person, schedule_machine, schedule_lighting, \
           total_value_lighting, convective_fraction_lighting, radiant_fraction_lighting, number_of_occupants, \
           convective_fraction_persons, radiant_fraction_persons, total_value_machines, convective_fraction_machines,\
           radiant_fraction_machines


def get_ade_floor_area(generalinfo):
    """floorAreaType the floorAreaValue"""
    for b, floor in enumerate(generalinfo.GenericApplicationPropertyOfAbstractBuilding):
        if isinstance(floor, energy.FloorAreaPropertyType):
            floorareavalue = floor.FloorArea.value_.value()
            # floorareauom = floor.FloorArea.value_.uom
            # floorareatype = floor.FloorArea.type
    return floorareavalue


def set_ade_bldgelements(tz, tzb_dict, tzb_dict_openings, layer_dict, material_dict, constr_dict, tz_factor=1):
    """Trying to set Rooftop / with Layers/ Materials"""
    #TODO: Do something nice for Air and other gasses as Materials
    for key, value in tzb_dict.items():
        if value[0] == "roof":
            roof = Rooftop(parent=tz)
            roof.name = key
            for key_openings, value_openings in tzb_dict_openings.items():
                if key == key_openings:
                    roof.area = (value[3] - value_openings[1]) * tz_factor # MultiZoneTest
                    break
                else:
                    roof.area = value[3] * tz_factor # MultiZoneTest
            roof.area = value[3] * tz_factor # MultiZoneTest
            roof.orientation = value[1]
            roof.tilt = value[2]
            for concof in constr_dict[value[4]]:
                if concof[0] is not None:
                    roof.outer_convection = concof[0]
                    # roof.outer_radiation = 0
                if concof[1] is not None:
                    roof.inner_convection = concof[1]
            for layers in layer_dict[value[4]]:
                layer = Layer(parent=roof, id=layers[0])
                layer.thickness = layers[2]
                material = Material(parent=layer)
                material.name = material_dict[layers[3]][0][0]
                if material_dict[layers[3]][0][0] == 'KIT-FZK-Haus-Luftschicht' or \
                    material_dict[layers[3]][0][0] == 'Bau05-Material-Air':
                    rvalue = material_dict[layers[3]][0][2]
                    material.thermal_conduc = 0.02225
                    material.density = 1.2041
                    material.heat_capac = 1
                else:
                    material.density = material_dict[layers[3]][0][1]
                    material.thermal_conduc = material_dict[layers[3]][0][2]
                    material.heat_capac = material_dict[layers[3]][0][3]
                BuildingElement.add_layer(roof, layer=layer)

            # roof.load_type_element(year=bldg.year_of_construction, construction='heavy')

    """trying to set the Outer Wall / with Layers/ Materials"""

    for key, value in tzb_dict.items():
        if value[0] == "outerWall":
            out_wall = OuterWall(parent=tz)
            out_wall.name = key
            openings_area_for_surface = 0
            # TODO: check again for correctness
            for key_openings, value_openings in tzb_dict_openings.items():
                if key == value_openings[0]:
                    openings_area_for_surface += value_openings[1]
            out_wall.area = (value[3] - openings_area_for_surface) * tz_factor # MultiZoneTest
            print(out_wall.area, value[3])
                # else:
                #     out_wall.area = value[3] * tz_factor
            out_wall.orientation = value[1]
            out_wall.tilt = value[2]
            for concof in constr_dict[value[4]]:
                if concof[0] is not None:
                    out_wall.outer_convection = concof[0]
                    # out_wall.outer_radiation = 0
                if concof[1] is not None:
                    out_wall.inner_convection = concof[1]
            for layers in layer_dict[value[4]]:
                layer = Layer(parent=out_wall, id=layers[0])
                layer.thickness = layers[2]
                material = Material(parent=layer)
                material.name = material_dict[layers[3]][0][0]
                if material_dict[layers[3]][0][0] == 'KIT-FZK-Haus-Luftschicht' or \
                        material_dict[layers[3]][0][0] == 'Bau05-Material-Air':
                    print("went through here")
                    rvalue = material_dict[layers[3]][0][2]
                    material.thermal_conduc = 0.02225
                    material.density = 1.2041
                    material.heat_capac = 1
                else:
                    material.density = material_dict[layers[3]][0][1]
                    material.thermal_conduc = material_dict[layers[3]][0][2]
                    material.heat_capac = material_dict[layers[3]][0][3]
                BuildingElement.add_layer(out_wall, layer=layer)

            # out_wall.load_type_element(year=bldg.year_of_construction, construction='heavy')

    """trying to set the Ground Floor / with Layers/ Materials"""

    for key, value in tzb_dict.items():
        if value[0] == "groundSlab":
            ground = GroundFloor(parent=tz)
            ground.name = key
            ground.area = value[3] * tz_factor # MultiZoneTest
            ground.orientation = value[1]
            ground.tilt = value[2]
            for concof in constr_dict[value[4]]:
                if concof[0] is not None:
                    ground.outer_convection = concof[0]
                    # ground.outer_radiation = 0
                if concof[1] is not None:
                    ground.inner_convection = concof[1]
            for layers in layer_dict[value[4]]:
                layer = Layer(parent=ground, id=layers[0])
                layer.thickness = layers[2]
                material = Material(parent=layer)
                material.name = material_dict[layers[3]][0][0]
                if material_dict[layers[3]][0][0] == 'KIT-FZK-Haus-Luftschicht' or \
                        material_dict[layers[3]][0][0] == 'Bau05-Material-Air':
                    rvalue = material_dict[layers[3]][0][2]
                    material.thermal_conduc = 0.02225
                    material.density = 1.2041
                    material.heat_capac = 1
                else:
                    material.density = material_dict[layers[3]][0][1]
                    material.thermal_conduc = material_dict[layers[3]][0][2]
                    material.heat_capac = material_dict[layers[3]][0][3]
                BuildingElement.add_layer(ground, layer=layer)

            # ground.load_type_element(year=bldg.year_of_construction, construction='heavy')

    """trying to set the Windows"""
    # todo how to use the information that is given:, glazingratio...

    for key, value in tzb_dict_openings.items():
        if value[4] == str("Door_Construction"):
            door = Door(parent=tz)
            door.name = key
            door.area = value[1]
            door.tilt = value[2]
            door.orientation = value[3]
            for layers in layer_dict[value[4]]:
                layer = Layer(parent=door, id=layers[0])
                layer.thickness = layers[2]
                material = Material(parent=layer)
                material.name = material_dict[layers[3]][0][0]
                material.density = material_dict[layers[3]][0][1]
                material.thermal_conduc = material_dict[layers[3]][0][2]
                material.heat_capac = material_dict[layers[3]][0][3]
            BuildingElement.add_layer(door, layer=layer)
        else:
            win = Window(parent=tz)
            win.area = value[1]
            win.tilt = value[2]
            win.orientation = value[3]
            # win.u_value = layers[1]
            print(value[4])
            for layers in layer_dict[value[4]]:
                win.g_value = 0.7
                win.a_conv = 0.3
                win.shading_g_total = 1
                win.shading_max_irr = 0.9
                win.name = layers[0]
                layer = Layer(parent=win)
                layer.id = value[0]
                layer.thickness = 0.34
                material = Material(parent=layer)
                material.transmittance = 0.3
                material.thermal_conduc = 0.96
                material.solar_absorp = 0.5
                material.density = 2579
            BuildingElement.add_layer(win, layer=layer)
        # win.load_type_element(year=bldg.year_of_construction, construction='heavy')


def set_ade_boundary_conditions(prj, tz, tz_id, type_of_usage, floorareavalue, schedule_heating, schedule_ventilation,
                                schedule_person, schedule_machine, schedule_lighting, total_value_lighting,
                                convective_fraction_lighting, radiant_fraction_lighting, number_of_occupants,
                                convective_fraction_persons, radiant_fraction_persons, total_value_machines,
                                convective_fraction_machines, radiant_fraction_machines):

    """Trying to set the Boundary Conditions"""
    # TODO: look once again over the Boundary Condition calculations and selection

    if tz.infiltration_rate is None:
        tz.infiltration_rate = sum(schedule_ventilation) / len(schedule_ventilation)

    tz.use_conditions = BoundaryConditions(parent=tz)
    tz.use_conditions.load_use_conditions(type_of_usage + "" + tz_id, prj.data)
    print(tz.use_conditions.usage)
    if tz.use_conditions.usage == "Single office":
        tz.use_conditions.load_use_conditions("Living", prj.data)
        tz.use_conditions.usage = type_of_usage + "" + tz_id
        BoundaryConditions.typical_length = np.sqrt(floorareavalue)
        BoundaryConditions.typical_width = np.sqrt(floorareavalue)
        BoundaryConditions.min_temp_heat = 273.15 + min(schedule_heating)
        BoundaryConditions.temp_set_back = max(schedule_heating) - min(schedule_heating)
        # BoundaryConditions.daily_usage_hours = 17
        BoundaryConditions.usage_time = [7, 23]
        BoundaryConditions.daily_operation_heating = 24
        BoundaryConditions.heating_time = [0, 23]
        BoundaryConditions.cooling_time = [0, 23]
        BoundaryConditions.max_temp_cool = 299.15
        BoundaryConditions.set_temp_heat = 273.15 + max(schedule_heating)
        BoundaryConditions.set_temp_cool = 299.15
        BoundaryConditions.min_air_exchange = tz.infiltration_rate

        """Occupants"""

        BoundaryConditions.persons = number_of_occupants
        BoundaryConditions.activity_type_persons = 0
        BoundaryConditions.ratio_conv_rad_persons = max(convective_fraction_persons, radiant_fraction_persons)

        """Machines"""
        # BoundaryConditions.machines = 7.82

        if total_value_machines * tz.area <= 50.0:
            tz.use_conditions.activity_type_machines = 1
        elif total_value_machines * tz.area > 50.0 and total_value_machines * tz.area <= 100.0:
            tz.use_conditions.activity_type_machines = 2
        else:
            tz.use_conditions.activity_type_machines = 3
        tz.use_conditions._ratio_conv_rad_machines = max(convective_fraction_machines, radiant_fraction_machines)

        """Lighting"""

        BoundaryConditions.lighting_power = total_value_lighting
        BoundaryConditions.ratio_conv_rad_lighting = max(convective_fraction_lighting, radiant_fraction_lighting)

        for (schedule, profile) in [(schedule_person, tz.use_conditions.profile_persons),
                                  (schedule_machine, tz.use_conditions.profile_machines),
                                  (schedule_lighting, tz.use_conditions.profile_lighting)]:
            print(schedule, profile)
            if not schedule:
                schedule = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                profile = schedule
            else:
                profile = schedule
            print(profile)
        tz.use_conditions.save_use_conditions(prj.data)
    else:
        tz.use_conditions.load_use_conditions(type_of_usage, prj.data)


def _set_attributes(bldg, gml_bldg, gml_bldg_part=None, bldg_part=None, bldg_yoc=None):
    """tries to set attributes for type building generation"""
    try:
        # bldg.name = gml_bldg.name[0].value()
        # bldg.name = gml_bldg.id
        if gml_bldg_part is None:
            try:
                bldg.name = gml_bldg.externalReference[0].externalObject.name
            except:
                bldg.name = gml_bldg.name[0].value()
        else:
            bldg.name = f'{gml_bldg.externalReference[0].externalObject.name}bt_{bldg_part}'
            print(bldg.name)
    except UserWarning:
        bldg.name = gml_bldg.id
        print("no name specified in gml file")
        pass
    try:
        bldg.number_of_floors = gml_bldg.storeysAboveGround
    except UserWarning:
        print("no storeysAboveGround specified in gml file")
        pass
    try:
        bldg.height_of_floors = gml_bldg.storeyHeightsAboveGround.value()[0]
    except (UserWarning, AttributeError):
        print("no storeyHeightsAboveGround specified in gml file")
        pass
    if bldg_yoc is None:
        try:
            bldg.year_of_construction = gml_bldg.yearOfConstruction.year
        except (UserWarning, AttributeError):
            print("no yearOfConstruction specified in gml file")
            print("default is set to 1980")
            bldg.year_of_construction = 1980
            pass
    else:
        bldg.year_of_construction = bldg_yoc
    if gml_bldg_part is None:
        try:
            bldg.bldg_height = gml_bldg.measuredHeight.value()
        except (UserWarning, AttributeError):
            print(gml_bldg.measuredHeight)
            print("no measuredHeight specified in gml file")
            pass
    else:
        try:
            bldg.bldg_height = gml_bldg_part.measuredHeight.value()
        except (UserWarning, AttributeError):
            print("no measuredHeight specified in gml file")
            pass


def _create_building(bldg, city_object):
    # LoD0
    if city_object.Feature.lod0FootPrint or city_object.Feature.lod0RoofEdge:
        if city_object.Feature.measuredHeight.value() is not None:
            from itertools import chain
            height = city_object.Feature.measuredHeight.value()
            base = city_object.Feature.lod0FootPrint.MultiSurface.surfaceMember[0]. \
                Surface.exterior.Ring.posList.value()
            roof = [base[0], base[1], base[2] + height, base[9], base[10], base[11] + height, base[6], base[7],
                    base[8] + height, base[3], base[4], base[5] + height, base[12], base[13], base[14] + height]

            help_list_base = list(zip(*[iter(base)] * 3))
            help_list_roof = list(zip(*[iter(roof)] * 3))

            wall_help_1 = [help_list_base[0], help_list_base[1], help_list_roof[3], help_list_roof[0],
                           help_list_base[0]]
            wall_list_1 = list(chain(*wall_help_1))

            wall_help_2 = [help_list_base[0], help_list_base[3], help_list_roof[1], help_list_roof[0],
                           help_list_base[0]]
            wall_list_2 = list(chain(*wall_help_2))

            wall_help_3 = [help_list_base[2], help_list_base[1], help_list_roof[3], help_list_roof[2],
                           help_list_base[2]]
            wall_list_3 = list(chain(*wall_help_3))

            wall_help_4 = [help_list_base[2], help_list_base[3], help_list_roof[1], help_list_roof[2],
                           help_list_base[2]]
            wall_list_4 = list(chain(*wall_help_4))

            bldg.gml_surfaces.append(SurfaceGML(base))
            bldg.gml_surfaces.append(SurfaceGML(roof))
            bldg.gml_surfaces.append(SurfaceGML(wall_list_1))
            bldg.gml_surfaces.append(SurfaceGML(wall_list_2))
            bldg.gml_surfaces.append(SurfaceGML(wall_list_3))
            bldg.gml_surfaces.append(SurfaceGML(wall_list_4))

        else:
            print("The LoD0 Model, no building-height is defined, set a height or no calculations are possible")

    # LOD1
    elif city_object.Feature.lod1Solid:
        for member in city_object.Feature.lod1Solid.Solid.exterior.Surface.surfaceMember:
            try:
                bldg.gml_surfaces.append(SurfaceGML(member.Surface.exterior.Ring.posList.value()))
            except AttributeError:
                position_list_help = []
                for pos in member.Surface.exterior.Ring.pos:
                    position_list_help += pos.value()
                help = SurfaceGML(position_list_help)
                bldg.gml_surfaces.append(help)


    # LOD2
    elif city_object.Feature.lod2Solid:
        for bound_surf in city_object.Feature.boundedBy_:
            for surf_member in bound_surf.BoundarySurface.lod2MultiSurface.MultiSurface.surfaceMember:
                if isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.CompositeSurfaceType):
                    for comp_member in surf_member.Surface.surfaceMember:
                        print(comp_member)
                        print(comp_member.Surface.exterior.Ring.posList)
                        try:  # modelling option 1
                            bldg.gml_surfaces.append(SurfaceGML(
                                comp_member.Surface.exterior.Ring.posList.value()))
                        except AttributeError:  # modelling option 2
                            position_list_help = []
                            for pos in comp_member.Surface.exterior.Ring.pos:
                                position_list_help += pos.value()
                            help = SurfaceGML(position_list_help)
                            bldg.gml_surfaces.append(help)
                else:
                    try:  # modelling option 1
                        bldg.gml_surfaces.append(SurfaceGML(
                            surf_member.Surface.exterior.Ring.posList.value()))

                    except AttributeError:  # modelling option 2
                        position_list_help = []
                        for pos in surf_member.Surface.exterior.Ring.pos:
                            position_list_help += pos.value()
                        help = SurfaceGML(position_list_help)
                        bldg.gml_surfaces.append(help)

    # LOD3
    elif city_object.Feature.lod3Solid or city_object.Feature.boundedBy_[5].BoundarySurface.lod3MultiSurface:
        for bound_by in city_object.Feature.boundedBy_:
            print(bound_by)
            for surf_member in bound_by.BoundarySurface.lod3MultiSurface.MultiSurface.surfaceMember:
                print(surf_member)
                if isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.CompositeSurfaceType):
                    for comp_member in surf_member.Surface.surfaceMember:
                        try:  # modelling option 1
                            bldg.gml_surfaces.append(SurfaceGML(
                                comp_member.Surface.exterior.Ring.posList.value()))
                        except AttributeError:  # modelling option 2
                            position_list_help = []
                            for pos_list in comp_member.Surface.exterior.Ring.pos:
                                position_list_help += pos_list.value()
                                print(position_list_help)
                            help = SurfaceGML(position_list_help)
                            if help.surface_area > 1:
                                help.name = bound_by.BoundarySurface.name[0].value()
                                bldg.gml_surfaces.append(help)
                else:
                    try:  # modelling option 1
                        bldg.gml_surfaces.append(SurfaceGML(
                            surf_member.Surface.exterior.Ring.posList.value()))
                        print(surf_member.Surface.exterior.Ring.posList.value())
                    except AttributeError:  # modelling option 2
                        position_list_help = []
                        for pos in surf_member.Surface.exterior.Ring.pos:
                            position_list_help += pos.value()
                        help = SurfaceGML(position_list_help)
                        if help.surface_area > 15:
                            help.name = bound_by.BoundarySurface.name[0].value()
                            bldg.gml_surfaces.append(help)


        # Windows and Doors
        for bound_surf in city_object.Feature.boundedBy_:
            for openings in bound_surf.BoundarySurface.opening:
                if isinstance(openings.Opening, teaserplus.data.bindings.opengis.citygml.raw.building.WindowType):
                    for surf_member in openings.Opening.lod3MultiSurface.MultiSurface.surfaceMember:
                        try:
                            help = SurfaceGML(surf_member.Surface.exterior.Ring.posList.value())
                            help.name = "Window"
                            bldg.gml_surfaces.append(help)
                        except AttributeError:  # modelling option 2
                            position_list_help = []
                            for pos in surf_member.Surface.exterior.Ring.pos:
                                position_list_help += pos.value()
                            help = SurfaceGML(position_list_help)
                            help.name = "Window"
                            bldg.gml_surfaces.append(help)
                else:
                    pass
                    # print(openings.Opening.description.value())

    # LOD4
    elif city_object.Feature.lod4Solid:
        for bound_by in city_object.Feature.boundedBy_:
            for surf_member in bound_by.BoundarySurface.lod4MultiSurface.MultiSurface.surfaceMember:
                if isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.CompositeSurfaceType):
                    for comp_member in surf_member.Surface.surfaceMember:
                        position_list_help = []
                        for pos_list in comp_member.Surface.exterior.Ring.pos:
                            position_list_help += pos_list.value()
                        help = SurfaceGML(position_list_help)
                        if help.surface_area > 1:
                            help.name = bound_by.BoundarySurface.name[0].value()
                            bldg.gml_surfaces.append(help)
                else:
                    position_list_help = []
                    for pos in surf_member.Surface.exterior.Ring.pos:
                        position_list_help += pos.value()
                    help = SurfaceGML(position_list_help)
                    if help.surface_area > 15:
                        help.name = bound_by.BoundarySurface.name[0].value()
                        bldg.gml_surfaces.append(help)

        # Windows and Doors
        for bound_surf in city_object.Feature.boundedBy_:
            for openings in bound_surf.BoundarySurface.opening:
                if isinstance(openings.Opening, teaserplus.data.bindings.opengis.citygml.raw.building.WindowType):
                    for surf_member in openings.Opening.lod4MultiSurface.MultiSurface.surfaceMember:
                        position_list_help = []
                        for pos in surf_member.Surface.exterior.Ring.pos:
                            position_list_help += pos.value()
                        help = SurfaceGML(position_list_help)
                    help.name = "Window"
                    bldg.gml_surfaces.append(help)
                else:
                    print(openings.Opening.name)

        # InteriorWalls
        for room in city_object.Feature.interiorRoom:
            for bound_by in room.Room.boundedBy_:
                for surf_member in bound_by.BoundarySurface.lod4MultiSurface.MultiSurface.surfaceMember:
                    if isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.CompositeSurfaceType):
                        for comp_member in surf_member.Surface.surfaceMember:
                            position_list_help = []
                            for pos_list in comp_member.Surface.exterior.Ring.pos:
                                position_list_help += pos_list.value()
                            help = SurfaceGML(position_list_help)
                            if help.surface_area > 1:
                                help.name = "interior " + room.Room.name[0].value() + " " + \
                                            bound_by.BoundarySurface.name[0].value()
                                print(help.name)
                                print(help.surface_area)
                                help.name = "InnerWall"
                                bldg.gml_surfaces.append(help)
                    elif isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.PolygonType):
                        position_list_help = []
                        for pos in surf_member.Surface.exterior.Ring.pos:
                            position_list_help += pos.value()
                        help = SurfaceGML(position_list_help)
                        if help.surface_area > 1:
                            help.name = "interior " + room.Room.name[0].value() + " " + \
                                        bound_by.BoundarySurface.name[0].value()
                            print(help.name)
                            print(help.surface_area)
                            help.name = "InnerWall"
                            bldg.gml_surfaces.append(help)
                    else:
                        print(surf_member.Surface.baseSurface.Surface)


def _create_building_part(bldg, part):
    # TODO: Add LoD3 and LoD4 to create_building_part

    # LOD1
    if part.BuildingPart.lod1Solid:
        for member in part.BuildingPart.lod1Solid.Solid.exterior.Surface.surfaceMember:
            try:
                bldg.gml_surfaces.append(SurfaceGML(member.Surface.exterior.Ring.posList.value()))
            except AttributeError:
                position_list_help = []
                for pos in member.Surface.exterior.Ring.pos:
                    position_list_help += pos.value()
                help = SurfaceGML(position_list_help)
                bldg.gml_surfaces.append(help)

    # LOD2
    elif part.BuildingPart.lod2Solid:
        for bound_surf in part.BuildingPart.boundedBy_:
            for surf_member in bound_surf.BoundarySurface.lod2MultiSurface.MultiSurface.surfaceMember:
                print(surf_member)
                if isinstance(surf_member.Surface, teaserplus.data.bindings.opengis.raw.gml.CompositeSurfaceType):
                    for comp_member in surf_member.Surface.surfaceMember:
                        print(comp_member)
                        print(comp_member.Surface.exterior.Ring.posList)
                        try:  # modelling option 1
                            bldg.gml_surfaces.append(SurfaceGML(
                                comp_member.Surface.exterior.Ring.posList.value()))
                        except AttributeError:  # modelling option 2
                            position_list_help = []
                            for pos in comp_member.Surface.exterior.Ring.pos:
                                position_list_help += pos.value()
                            help = SurfaceGML(position_list_help)
                            bldg.gml_surfaces.append(help)
                else:
                    try:  # modelling option 1
                        bldg.gml_surfaces.append(SurfaceGML(
                            surf_member.Surface.exterior.Ring.posList.value()))

                    except AttributeError:  # modelling option 2
                        position_list_help = []
                        for pos in surf_member.Surface.exterior.Ring.pos:
                            position_list_help += pos.value()
                        help = SurfaceGML(position_list_help)
                        bldg.gml_surfaces.append(help)


def _convert_bldg(bldg, function):
    """converts the instance to a specific archetype building

    DANGEROUS function, should only be used in combination with CityGML
    and if you know what you are doing

    Parameters
    ----------

    bldg : Building()
        TEASER instance of a building

    function : str
        function from CityGML code list 1000 is residential 1120 is office
    """
    parent_help = bldg.parent
    name_help = bldg.name
    gml_surfaces_help = bldg.gml_surfaces
    year_of_construction_help = bldg.year_of_construction
    bldg_height_help = bldg.bldg_height

    if function == "1000":
        from teaserplus.logic.archetypebuildings.bmvbs.singlefamilydwelling \
            import SingleFamilyDwelling
        bldg.__class__ = SingleFamilyDwelling
    elif function == "1120":
        from teaserplus.logic.archetypebuildings.bmvbs.office import Office
        bldg.__class__ = Office

    bldg.__init__(parent=None)
    bldg.gml_surfaces = gml_surfaces_help
    bldg.parent = parent_help
    bldg.name = name_help
    bldg.year_of_construction = year_of_construction_help
    bldg.bldg_height = bldg_height_help

    bldg.set_gml_attributes()
    bldg.generate_from_gml()


class SurfaceGML(object):
    """Class for calculating attributes of CityGML surfaces

    this class automatically calculates surface area using an algorithm for
    polygons with arbitrary number of points. The Surface orientation by
    analysing the normal vector (caution: the orientations are then set to
    TEASER orientation). The Surface tilt by analysing the normal vector.

    Parameters
    ----------

    gml_surface : list
        list of gml points with srsDimension=3 the first 3 and the last 3
        entries must describe the same point in CityGML

    boundary : str
        Name of the boundary surface

    """

    def __init__(self,
                 gml_surface,
                 boundary=None):
        self.gml_surface = gml_surface
        self.name = boundary
        self.surface_area = None
        self.surface_orientation = None
        self.surface_tilt = None

        self.surface_area = self.get_gml_area()
        self.surface_orientation = self.get_gml_orientation()
        self.surface_tilt = self.get_gml_tilt()

    def get_gml_area(self):
        """calc the area of a gml_surface defined by gml coordinates

        Surface needs to be planar

        Returns
        ----------
        surface_area : float
            returns the area of the surface
        """

        split_surface = list(zip(*[iter(self.gml_surface)] * 3))
        self.surface_area = self.poly_area(poly=split_surface)
        return self.surface_area

    def get_gml_tilt(self):
        """calc the tilt of a gml_surface defined by 4 or 5 gml coordinates

        Surface needs to be planar

        Returns
        ----------
        surface_tilt : float
            returns the orientation of the surface
        """

        gml_surface = np.array(self.gml_surface)
        gml1 = gml_surface[0:3]
        gml2 = gml_surface[3:6]
        gml3 = gml_surface[6:9]

        vektor_1 = gml2 - gml1
        vektor_2 = gml3 - gml1

        normal_1 = np.cross(vektor_1, vektor_2)
        z_axis = np.array([0, 0, 1])

        self.surface_tilt = np.arccos(np.dot(normal_1, z_axis) / (LA.norm(
            z_axis) * LA.norm(normal_1))) * 360 / (2 * np.pi)

        if self.surface_tilt == 180:
            self.surface_tilt = 0.0
        elif str(self.surface_tilt) == "nan":
            self.surface_tilt = None
        return self.surface_tilt

    def get_gml_orientation(self):
        """calc the orientation of a gml_surface defined by 4 or 5 gml
        coordinates

        Surface needs to be planar, the orientation returned is in TEASER
        coordinates

        Returns
        ----------
        surface_orientation : float
            returns the orientation of the surface
        """

        gml_surface = np.array(self.gml_surface)
        gml1 = gml_surface[0:3]
        gml2 = gml_surface[3:6]
        gml3 = gml_surface[6:9]
        gml4 = gml_surface[9:12]
        if len(gml_surface) > 12:
            vektor_1 = gml2 - gml1
            vektor_2 = gml4 - gml1
        else:
            vektor_1 = gml2 - gml1
            vektor_2 = gml3 - gml1

        normal_1 = np.cross(vektor_1, vektor_2)
        normal_uni = normal_1 / LA.norm(normal_1)
        phi = None
        if normal_uni[0] > 0:
            phi = np.arctan(normal_uni[1] / normal_uni[0])
        elif normal_uni[0] < 0 <= normal_uni[1]:
            phi = np.arctan(normal_uni[1] / normal_uni[0]) + np.pi
        elif normal_uni[0] < 0 > normal_uni[1]:
            phi = np.arctan(normal_uni[1] / normal_uni[0]) - np.pi
        elif normal_uni[0] == 0 < normal_uni[1]:
            phi = np.pi / 2
        elif normal_uni[0] == 0 > normal_uni[1]:
            phi = -np.pi / 2

        if phi is None:
            pass
        elif phi < 0:
            self.surface_orientation = (phi + 2 * np.pi) * 360 / (2 * np.pi)
        else:
            self.surface_orientation = phi * 360 / (2 * np.pi)

        if self.surface_orientation is None:
            pass
        elif 0 <= self.surface_orientation <= 90:
            self.surface_orientation = 90 - self.surface_orientation
        else:
            self.surface_orientation = 450 - self.surface_orientation

        if normal_uni[2] == -1:
            self.surface_orientation = -2
        elif normal_uni[2] == 1:
            self.surface_orientation = -1
        return self.surface_orientation

    def unit_normal(self, a, b, c):
        """calculates the unit normal vector of a surface described by 3 points

        Parameters
        ----------

        a : float
            point 1
        b : float
            point 2
        c : float
            point 3

        Returns
        ----------

        unit_normal : list
            unit normal vector as a list

        """

        x = np.linalg.det([[1, a[1], a[2]],
                           [1, b[1], b[2]],
                           [1, c[1], c[2]]])
        y = np.linalg.det([[a[0], 1, a[2]],
                           [b[0], 1, b[2]],
                           [c[0], 1, c[2]]])
        z = np.linalg.det([[a[0], a[1], 1],
                           [b[0], b[1], 1],
                           [c[0], c[1], 1]])
        magnitude = (x**2 + y**2 + z**2)**.5
        return x / magnitude, y / magnitude, z / magnitude

    def poly_area(self, poly):
        """calculates the area of a polygon with arbitrary points

        Parameters
        ----------

        poly : list
            polygon as a list in srsDimension = 3

        Returns
        ----------

        area : float
            returns the area of a polygon
        """

        if len(poly) < 3:  # not a plane - no area
            return 0
        total = [0, 0, 0]
        length = len(poly)
        for i in range(length):
            vi1 = poly[i]
            vi2 = poly[(i + 1) % length]
            prod = np.cross(vi1, vi2)
            total[0] += prod[0]
            total[1] += prod[1]
            total[2] += prod[2]
        result = np.dot(total, self.unit_normal(poly[0], poly[1], poly[2]))
        return abs(result / 2)
